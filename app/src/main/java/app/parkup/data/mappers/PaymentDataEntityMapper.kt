package app.parkup.data.mappers

import app.parkup.data.entities.PaymentData
import app.parkup.data.entities.UserData
import app.parkup.data.entities.VehicleData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.entities.VehicleEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PaymentDataEntityMapper @Inject constructor() : Mapper<PaymentData, PaymentEntity>() {
    override fun mapFrom(from: PaymentData): PaymentEntity {
        return PaymentEntity(
            id = from.id,
            amount = from.amount,
            cancellation_reason = from.cancellation_reason,
            capture_method = from.capture_method,
            canceled_at = from.canceled_at ,
            status = from.status
        )
    }
}
