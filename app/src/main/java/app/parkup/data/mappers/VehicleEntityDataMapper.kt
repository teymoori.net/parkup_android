package app.parkup.data.mappers

import app.parkup.data.entities.UserData
import app.parkup.data.entities.VehicleData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.entities.VehicleEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VehicleEntityDataMapper @Inject constructor() : Mapper<VehicleEntity, VehicleData>() {
    override fun mapFrom(from: VehicleEntity): VehicleData {
        return VehicleData(
            id = from.id,
            color = from.color,
            color_rgb = from.color_rgb,
            created_at = from.created_at,
            model = from.model,
            plate_number = from.plate_number,
            default_vehicle = from.default_vehicle
        )
    }
}
