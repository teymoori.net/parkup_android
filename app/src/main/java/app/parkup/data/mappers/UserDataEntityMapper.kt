package app.parkup.data.mappers

import app.parkup.data.entities.UserData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.UserEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDataEntityMapper @Inject constructor() : Mapper<UserData, UserEntity>() {
    override fun mapFrom(from: UserData): UserEntity {
        return UserEntity(
            id = from.id,
            access_token = from.access_token,
            email = from.email,
            family = from.family,
            gender = from.gender,
            name = from.name,
            phone_number = from.phone_number,
            role_id = from.role_id
        )
    }
}
