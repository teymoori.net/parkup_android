package app.parkup.data.mappers

import app.parkup.data.entities.MessageData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.MessageEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MessageEntityMapper
@Inject constructor() : Mapper<MessageData, MessageEntity>() {

    override fun mapFrom(from: MessageData): MessageEntity {
        return MessageEntity(
            message = from.message
        )
    }
}
