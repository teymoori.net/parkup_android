package app.parkup.data.mappers

import app.parkup.data.entities.PriceParkingData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.PriceParkingEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PriceParkingDataEntityMapper
@Inject constructor() : Mapper<PriceParkingData, PriceParkingEntity>() {
    override fun mapFrom(from: PriceParkingData): PriceParkingEntity {
        return PriceParkingEntity(
            type = from.type,
            title = from.title,
            cost = from.cost,
            hours_expiry = from.hours_expiry
        )
    }
}
