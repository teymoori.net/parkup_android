package app.parkup.data.mappers

import app.parkup.data.entities.ForgetPasswordData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.ForgetPasswordEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ForgetPasswordEntityMapper
@Inject constructor() : Mapper<ForgetPasswordData, ForgetPasswordEntity>() {

    override fun mapFrom(from: ForgetPasswordData): ForgetPasswordEntity {
        return ForgetPasswordEntity(
            message = from.message ,
            hash = from.hash
        )
    }
}
