package app.parkup.data.mappers

import app.parkup.data.entities.StripeSecretData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.StripeSecretEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StripeDataEntityMapper
@Inject constructor() : Mapper<StripeSecretData, StripeSecretEntity>() {
    override fun mapFrom(from: StripeSecretData): StripeSecretEntity {
        return StripeSecretEntity(
            client_secret = from.client_secret,
            email = from.email,
            name = from.name,
            phone_number = from.phone_number,
            publishable_key = from.publishable_key
        )
    }

}



