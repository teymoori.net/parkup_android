package app.parkup.data.mappers

import app.parkup.data.entities.PaymentMethodData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.PaymentMethodEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PaymentMethodDataEntityMapper
@Inject constructor() : Mapper<PaymentMethodData, PaymentMethodEntity>() {
    override fun mapFrom(from: PaymentMethodData): PaymentMethodEntity {
        return PaymentMethodEntity(
            id = from.id,
            brand = from.brand,
            exp_month = from.exp_month,
            exp_year = from.exp_year,
            fingerprint = from.fingerprint,
            funding = from.funding,
            last4 = from.last4,
            name = from.name,
            is_default = from.is_default
        )
    }


}



