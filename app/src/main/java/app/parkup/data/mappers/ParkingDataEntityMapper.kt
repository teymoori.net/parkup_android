package app.parkup.data.mappers

import app.parkup.data.entities.ParkingData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.ParkingEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ParkingDataEntityMapper
@Inject constructor() : Mapper<ParkingData, ParkingEntity>() {

    private val priceDataToEntityMapper = PriceParkingDataEntityMapper()

    override fun mapFrom(from: ParkingData): ParkingEntity {
        return ParkingEntity(
            id = from.id,
            area = from.area,
            daily_cost = from.daily_cost,
            email = from.email,
            full_address = from.full_address,
            image = from.image,
            is_open = from.is_open == true,
            latitude = from.latitude,
            longitude = from.longitude,
            manager = from.manager,
            monthly_cost = from.monthly_cost,
            phone = from.phone,
            rank = from.rank,
            title = from.title,
            created_at = from.created_at,
            distance = from.distance,
            updated_at = from.updated_at,
            one_hour_cost = from.one_hour_cost,
            accept_hourly = from.accept_hourly,
            max_hours_accept = from.max_hours_accept,
            cost = from.cost,
            start = from.start,
            end = from.end,
            duration_text = from.duration_text,
            is_attended = from.is_attended,
            is_covered = from.is_covered,
            is_valet = from.is_valet,
            hours_operation = from.hours_operation,
            description = from.description,
            prices = from.prices?.map { priceDataToEntityMapper.mapFrom(it) }
        )
    }


}



