package app.parkup.data.mappers

import app.parkup.data.entities.APICallResponseData
import app.parkup.data.entities.OrderData
import app.parkup.data.entities.ParkingData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.APICallResponseEntity
import app.parkup.domain.entities.ParkingEntity
import app.parkup.utils.tools.OrderEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class APIResponseCallDataEntityMapper
@Inject constructor() : Mapper<APICallResponseData, APICallResponseEntity>() {

    override fun mapFrom(from: APICallResponseData): APICallResponseEntity {
        return APICallResponseEntity(
            error = from.error,
            hasError = from.hasError,
            succeed = from.succeed
        )
    }
}
