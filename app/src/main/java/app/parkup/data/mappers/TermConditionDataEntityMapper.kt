package app.parkup.data.mappers

import app.parkup.data.entities.TermConditionData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.TermConditionEntitiy
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TermConditionDataEntityMapper
@Inject constructor() : Mapper<TermConditionData, TermConditionEntitiy>() {
    override fun mapFrom(from: TermConditionData): TermConditionEntitiy {
        return TermConditionEntitiy(
            title = from.title,
            detail = from.detail,
            updated_at = from.updated_at
        )
    }
}
