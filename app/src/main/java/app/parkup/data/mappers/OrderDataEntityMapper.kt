package app.parkup.data.mappers

import app.parkup.data.entities.OrderData
import app.parkup.data.entities.ParkingData
import app.parkup.domain.common.Mapper
import app.parkup.domain.entities.ParkingEntity
import app.parkup.utils.tools.OrderEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderDataEntityMapper
@Inject constructor() : Mapper<OrderData, OrderEntity>() {

    private val parkingDataEntityMapper = ParkingDataEntityMapper()
    private val vehicleDataEntityMapper = VehicleDataEntityMapper()

    override fun mapFrom(from: OrderData): OrderEntity {
        return OrderEntity(
            id = from.id,
            check_in_time = from.check_in_time,
            check_out_time = from.check_out_time,
            cost = from.cost,
            created_at = from.created_at,
            duration_text = from.duration_text,
            order_code = from.order_code,
            order_status = from.order_status,
            parking = parkingDataEntityMapper.mapFrom(from.parking),
            vehicle = vehicleDataEntityMapper.mapFrom(from.vehicle),
            updated_at = from.updated_at,
            customer_name = from.customer_name,
            customer_phone = from.customer_phone,
            vehicle_plate_number = from.vehicle_plate_number,
            user = from.user
        )
    }
}



