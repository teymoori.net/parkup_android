package app.parkup.data.repositories.login

import app.parkup.domain.common.Optional
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_login.UserCache
import app.parkup.utils.tools.DataProvider
import io.reactivex.Observable
import javax.inject.Inject

class UserCacheImpl @Inject
constructor(
    private val dataProvider: DataProvider
) : UserCache {
    companion object {
        const val USER_KEY = "user"
    }

    override fun isUserLoggedIn(): Observable<Boolean> {
        val user = dataProvider.getData(USER_KEY) as? UserEntity
        return Observable.just(user != null)
    }

    override fun setUser(user: UserEntity): Observable<Boolean> {
        dataProvider.saveData(USER_KEY, user)
        return Observable.just(true)
    }

    override fun getUser(): Observable<Optional<UserEntity>> {
        return Observable.fromCallable {
            return@fromCallable Optional.of(dataProvider.getData(USER_KEY) as? UserEntity)
        }
    }

    override fun removeUser(): Observable<Boolean> {
        dataProvider.removeData(USER_KEY)
        return Observable.just(true)
    }

}