package app.parkup.data.repositories.login

import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_login.UserCache
import app.parkup.domain.interactors.user_login.UserLoginRepository
import io.reactivex.Observable

class UserLoginRepositoryImpl
constructor(
    private val userLoginRemoteRepository: UserLoginRemoteRepository,
    private val userCache: UserCache
) : UserLoginRepository {

    override fun login(username: String, password: String): Observable<UserEntity> {
        return userLoginRemoteRepository.login(username, password).doOnNext { user ->
            if (user.access_token?.isNotEmpty() == true) {
                userCache.setUser(user)
            }
        }
    }
}