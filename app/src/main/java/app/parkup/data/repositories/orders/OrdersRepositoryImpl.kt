package app.parkup.data.repositories.orders

import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.domain.interactors.user_login.UserCache
import app.parkup.domain.interactors.user_login.UserLoginRepository
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

class OrdersRepositoryImpl
constructor(
    private val ordersRemoteRepository: OrdersRemoteRepository
) : OrdersRepository {
    override fun getOrder(id: Int): Observable<OrderEntity> {
        return ordersRemoteRepository.getOrder(id)
    }

    override fun getOrders(): Observable<List<OrderEntity>> {
        return ordersRemoteRepository.getOrders()
    }

    override fun searchOrder(word: String): Observable<List<OrderEntity>> {
        return ordersRemoteRepository.searchOrder(word)
    }

    override fun addNewOrder(request: AddOrderRequestData): Observable<OrderEntity> {
        return ordersRemoteRepository.addNewOrder(request)
    }

    override fun payOrder(orderID: Long): Observable<OrderEntity> {
        return ordersRemoteRepository.payOrder(orderID)
    }
}