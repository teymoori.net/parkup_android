package app.parkup.data.repositories.get_parkings

import app.parkup.data.api.Api
import app.parkup.data.entities.LoginRequestData
import app.parkup.data.entities.ParkingRequestData
import app.parkup.data.mappers.ParkingDataEntityMapper
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.interactors.get_parkings.GetParkingsRepository
import io.reactivex.Observable

class GetParkingsRemoteRepository
constructor(
    private val api: Api
) : GetParkingsRepository{

    private val parkingDataMapper = ParkingDataEntityMapper()

    override fun getParkings(request:ParkingRequestData): Observable<List<ParkingEntity>> {
        return api.getParkings(latitude = request.latitude , longitude = request.longitude).map { results ->
            results.map { parkingDataMapper.mapFrom(it) }
        }
    }

    override fun getParking(id: Int): Observable<ParkingEntity> {
        return api.getParking(id).flatMap { user ->
            Observable.just(parkingDataMapper.mapFrom(user))
        }
    }
}