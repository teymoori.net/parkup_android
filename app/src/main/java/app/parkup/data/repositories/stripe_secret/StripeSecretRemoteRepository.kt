package app.parkup.data.repositories.stripe_secret

import app.parkup.data.api.Api
import app.parkup.data.mappers.StripeDataEntityMapper
import app.parkup.data.mappers.UserDataEntityMapper
import app.parkup.domain.entities.StripeSecretEntity
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.stripe_secret.StripeSecretRepository
import app.parkup.domain.interactors.user_details.UserDetailsRepository
import io.reactivex.Observable

class StripeSecretRemoteRepository
constructor(
    private val api: Api
) : StripeSecretRepository {

    private val stripeDataEntityMapper = StripeDataEntityMapper()
    override fun getSecrets(): Observable<StripeSecretEntity> {
        return api.getStripeSecrets().flatMap { data ->
            Observable.just(stripeDataEntityMapper.mapFrom(data))
        }
    }
}