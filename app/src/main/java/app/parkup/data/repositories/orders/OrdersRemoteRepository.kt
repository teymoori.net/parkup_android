package app.parkup.data.repositories.orders

import app.parkup.data.api.Api
import app.parkup.data.entities.AddOrderRequestData
import app.parkup.data.entities.LoginRequestData
import app.parkup.data.mappers.OrderDataEntityMapper
import app.parkup.data.mappers.UserDataEntityMapper
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.domain.interactors.user_login.UserLoginRepository
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

class OrdersRemoteRepository
constructor(
    private val api: Api
) : OrdersRepository {

    private val orderDataEntityMapper = OrderDataEntityMapper()

    override fun getOrder(id: Int): Observable<OrderEntity> {
        return api.getOrder(id).flatMap { order ->
            Observable.just(orderDataEntityMapper.mapFrom(order))
        }
    }

    override fun getOrders(): Observable<List<OrderEntity>> {
        return api.getOrders().map { results ->
            results.map { orderDataEntityMapper.mapFrom(it) }
        }
    }

    override fun searchOrder(word: String): Observable<List<OrderEntity>> {
        return api.searchOrder(word).map { results ->
            results.map { orderDataEntityMapper.mapFrom(it) }
        }
    }

    override fun addNewOrder(request: AddOrderRequestData): Observable<OrderEntity> {
        return api.addOrder(request).flatMap { order ->
            Observable.just(orderDataEntityMapper.mapFrom(order))
        }
    }

    override fun payOrder(orderID: Long): Observable<OrderEntity> {
        return Observable.error { IllegalArgumentException("Username/Password must be provided.") }
    }
}