package app.parkup.data.repositories.register

import app.parkup.data.mappers.UserDataEntityMapper
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_login.UserCache
import app.parkup.domain.interactors.user_register.UserRegisterRepository
import io.reactivex.Observable

class UserRegisterRepositoryImpl
constructor(
    private val userRegisterRemoteRepository: UserRegisterRemoteRepository,
    private val userCache: UserCache
) : UserRegisterRepository {


    private val userDataMapper = UserDataEntityMapper()

    override fun register(
        name_family: String,
        email: String,
        password: String
    ): Observable<UserEntity> {
        return userRegisterRemoteRepository.register(
            name_family = name_family, email = email, password = password
        ).doOnNext { user ->
            if (user.access_token?.isNotEmpty() == true) {
                userCache.setUser(user)
            }
        }
    }
}