package app.parkup.data.repositories.terms

import app.parkup.data.api.Api
import app.parkup.data.mappers.TermConditionDataEntityMapper
import app.parkup.domain.entities.TermConditionEntitiy
import app.parkup.domain.interactors.terms.TermsConditionsRepository
import io.reactivex.Observable
import javax.inject.Inject

class TermsConditionsRepositoryImpl @Inject
constructor(
    private val api: Api
) : TermsConditionsRepository {

    private val mapper = TermConditionDataEntityMapper()

    override fun getTermsConditions(): Observable<List<TermConditionEntitiy>> {
        return api.getTermsConditions().map { results ->
            results.map { mapper.mapFrom(it) }
        }
    }
}