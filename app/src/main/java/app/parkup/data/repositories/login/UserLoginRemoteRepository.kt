package app.parkup.data.repositories.login

import app.parkup.data.api.Api
import app.parkup.data.entities.LoginRequestData
import app.parkup.data.mappers.UserDataEntityMapper
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_login.UserLoginRepository
import io.reactivex.Observable

class UserLoginRemoteRepository
constructor(
    private val api: Api
) : UserLoginRepository {

    private val userDataMapper = UserDataEntityMapper()

    override fun login(username: String, password: String): Observable<UserEntity> {
        return api.login(LoginRequestData(username, password)).flatMap { user ->
            Observable.just(userDataMapper.mapFrom(user))
        }
    }
}