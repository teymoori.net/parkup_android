package app.parkup.data.repositories.vehicles

import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class VehiclesRepositoryImpl
constructor(
    private val vehiclesRemoteRepository: VehiclesRemoteRepository
) : VehiclesRepository {


    override fun getVehicles(): Observable<List<VehicleEntity>> {
        return vehiclesRemoteRepository.getVehicles()
    }

    override fun addVehicle(vehicle: VehicleEntity): Observable<VehicleEntity> {
        return vehiclesRemoteRepository.addVehicle(vehicle)
    }

    override fun removeVehicle(vehicle: VehicleEntity): Observable<Boolean> {
        return vehiclesRemoteRepository.removeVehicle(vehicle)
    }

    override fun setDefaultVehicle(id: Int): Observable<VehicleEntity> {
        return vehiclesRemoteRepository.setDefaultVehicle(id)
    }
}