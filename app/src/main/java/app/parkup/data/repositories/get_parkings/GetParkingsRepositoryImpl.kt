package app.parkup.data.repositories.get_parkings

import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.interactors.get_parkings.GetParkingsRepository
import io.reactivex.Observable

class GetParkingsRepositoryImpl
constructor(
    private val repository: GetParkingsRemoteRepository
) : GetParkingsRepository {

    override fun getParkings(request: ParkingRequestData): Observable<List<ParkingEntity>> {
        return repository.getParkings(request)
    }

    override fun getParking(id: Int): Observable<ParkingEntity> {
        return repository.getParking(id)
    }

}