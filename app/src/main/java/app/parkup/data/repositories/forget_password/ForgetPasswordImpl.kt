package app.parkup.data.repositories.forget_password

import app.parkup.data.api.Api
import app.parkup.data.entities.ResetPasswordRequestData
import app.parkup.data.mappers.ForgetPasswordEntityMapper
import app.parkup.data.mappers.MessageEntityMapper
import app.parkup.domain.entities.ForgetPasswordEntity
import app.parkup.domain.entities.MessageEntity
import app.parkup.domain.interactors.forget_password.ForgetPasswordRepository
import io.reactivex.Observable
import javax.inject.Inject

class ForgetPasswordImpl @Inject
constructor(
    private val api: Api
) : ForgetPasswordRepository {

    private val forgetPasswordEntityMapper = ForgetPasswordEntityMapper()
    private val messageEntityMapper = MessageEntityMapper()

    override fun requestCode(username: String): Observable<ForgetPasswordEntity> {
        return api.forgetPassword(username).flatMap { data ->
            Observable.just(forgetPasswordEntityMapper.mapFrom(data))
        }
    }

    override fun resetPassword(request: ResetPasswordRequestData): Observable<MessageEntity> {
        return api.resetPassword(request).flatMap { data ->
            Observable.just(messageEntityMapper.mapFrom(data))
        }
    }
}