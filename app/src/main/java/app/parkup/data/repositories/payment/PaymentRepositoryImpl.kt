package app.parkup.data.repositories.payment

import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.interactors.payment_methods.PaymentRepository
import io.reactivex.Observable

class PaymentRepositoryImpl
constructor(
    private val paymentRemoteRepository: PaymentRemoteRepository
) : PaymentRepository {

    override fun pay(orderID: Long): Observable<PaymentEntity> {
        return paymentRemoteRepository.pay(orderID)
    }
}