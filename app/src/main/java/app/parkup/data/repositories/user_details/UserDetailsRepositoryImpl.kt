package app.parkup.data.repositories.user_details

import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_details.UserDetailsRepository
import io.reactivex.Observable

class UserDetailsRepositoryImpl
constructor(
    private val userDetailsRemoteRepository: UserDetailsRemoteRepository
) : UserDetailsRepository {

    override fun getDetails(): Observable<UserEntity> {
        return userDetailsRemoteRepository.getDetails()
    }
}