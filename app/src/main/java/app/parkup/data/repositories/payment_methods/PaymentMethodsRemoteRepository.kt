package app.parkup.data.repositories.payment_methods

import app.parkup.data.api.Api
import app.parkup.data.entities.PaymentMethodRequestData
import app.parkup.data.entities.PaymentRequestData
import app.parkup.data.mappers.APIResponseCallDataEntityMapper
import app.parkup.data.mappers.PaymentMethodDataEntityMapper
import app.parkup.domain.entities.APICallResponseEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.interactors.payment_methods.PaymentMethodsRepository
import io.reactivex.Observable

class PaymentMethodsRemoteRepository
constructor(
    private val api: Api
) : PaymentMethodsRepository {

    private val paymentMethodDataEntityMapper = PaymentMethodDataEntityMapper()
    private val apiResponseMapper = APIResponseCallDataEntityMapper()

    override fun getMethods(): Observable<List<PaymentMethodEntity>> {
        return api.getPaymentMethods().map { results ->
            results.map { paymentMethodDataEntityMapper.mapFrom(it) }
        }

    }

    override fun setDefaultMethod(paymentMethodID: String): Observable<APICallResponseEntity> {
        return api.setDefaultPaymentMethod(PaymentMethodRequestData(paymentMethodID)).flatMap { detail ->
            Observable.just(apiResponseMapper.mapFrom(detail))
        }
    }

    override fun removeMethod(paymentMethodID: String): Observable<APICallResponseEntity> {
        return api.removePaymentMethod(PaymentMethodRequestData(paymentMethodID)).flatMap { detail ->
            Observable.just(apiResponseMapper.mapFrom(detail))
        }
    }
}