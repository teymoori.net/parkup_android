package app.parkup.data.repositories.payment_methods

import app.parkup.domain.entities.APICallResponseEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.interactors.payment_methods.PaymentMethodsRepository
import io.reactivex.Observable

class PaymentMethodsRepositoryImpl
constructor(
    private val paymentMethodsRemoteRepository: PaymentMethodsRemoteRepository
) : PaymentMethodsRepository {

    override fun getMethods(): Observable<List<PaymentMethodEntity>> {
        return paymentMethodsRemoteRepository.getMethods()
    }

    override fun setDefaultMethod(paymentMethodID: String): Observable<APICallResponseEntity> {
        return paymentMethodsRemoteRepository.setDefaultMethod(paymentMethodID)
    }

    override fun removeMethod(paymentMethodID: String): Observable<APICallResponseEntity> {
        return paymentMethodsRemoteRepository.removeMethod(paymentMethodID)
    }
}