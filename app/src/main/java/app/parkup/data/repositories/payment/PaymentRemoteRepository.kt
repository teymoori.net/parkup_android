package app.parkup.data.repositories.payment

import app.parkup.data.api.Api
import app.parkup.data.entities.PaymentRequestData
import app.parkup.data.mappers.PaymentDataEntityMapper
import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.interactors.payment_methods.PaymentRepository
import io.reactivex.Observable

class PaymentRemoteRepository
constructor(
    private val api: Api
) : PaymentRepository {

    private val paymentDataEntityMapper = PaymentDataEntityMapper()


    override fun pay(orderID: Long): Observable<PaymentEntity> {
        return api.pay(PaymentRequestData(orderID)).flatMap { payment ->
            Observable.just(paymentDataEntityMapper.mapFrom(payment))
        }
    }
}