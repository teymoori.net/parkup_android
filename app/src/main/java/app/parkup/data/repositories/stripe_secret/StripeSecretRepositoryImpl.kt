package app.parkup.data.repositories.stripe_secret

import app.parkup.domain.entities.StripeSecretEntity
import app.parkup.domain.interactors.stripe_secret.StripeSecretRepository
import io.reactivex.Observable

class StripeSecretRepositoryImpl
constructor(
    private val repository: StripeSecretRepository
) : StripeSecretRepository {
    override fun getSecrets(): Observable<StripeSecretEntity> {
        return repository.getSecrets()
    }
}