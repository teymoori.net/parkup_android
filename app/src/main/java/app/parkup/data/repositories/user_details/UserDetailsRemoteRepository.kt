package app.parkup.data.repositories.user_details

import app.parkup.data.api.Api
import app.parkup.data.mappers.UserDataEntityMapper
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_details.UserDetailsRepository
import io.reactivex.Observable

class UserDetailsRemoteRepository
constructor(
    private val api: Api
) : UserDetailsRepository {

    private val userDataMapper = UserDataEntityMapper()

    override fun getDetails(): Observable<UserEntity> {
        return api.getUserDetails().flatMap { user ->
            Observable.just(userDataMapper.mapFrom(user))
        }
    }
}