package app.parkup.data.repositories.vehicles

import app.parkup.data.api.Api
import app.parkup.data.entities.LoginRequestData
import app.parkup.data.mappers.VehicleDataEntityMapper
import app.parkup.data.mappers.VehicleEntityDataMapper
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class VehiclesRemoteRepository
constructor(
    private val api: Api
) : VehiclesRepository {

    private val vehicleDataEntityMapper = VehicleDataEntityMapper()
    private val vehicleEntityDataMapper = VehicleEntityDataMapper()


    override fun getVehicles(): Observable<List<VehicleEntity>> {
        return api.getVehicles().map { results ->
            results.map { vehicleDataEntityMapper.mapFrom(it) }
        }
    }

    override fun addVehicle(vehicle: VehicleEntity): Observable<VehicleEntity> {
        return api.addVehicle(vehicleEntityDataMapper.mapFrom(vehicle)).flatMap { user ->
            Observable.just(vehicleDataEntityMapper.mapFrom(user))
        }
    }

    override fun removeVehicle(vehicle: VehicleEntity): Observable<Boolean> {
        return api.removeVehicle(vehicle.id ?: 0).flatMap {
            Observable.just(true)
        }
    }

    override fun setDefaultVehicle(id: Int): Observable<VehicleEntity> {
        return api.setDefaultVehicle(id).flatMap { user ->
            Observable.just(vehicleDataEntityMapper.mapFrom(user))
        }
    }
}