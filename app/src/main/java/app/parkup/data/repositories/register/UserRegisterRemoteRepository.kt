package app.parkup.data.repositories.register

import app.parkup.data.api.Api
import app.parkup.data.entities.RegisterRequestData
import app.parkup.data.mappers.UserDataEntityMapper
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_register.UserRegisterRepository
import io.reactivex.Observable

class UserRegisterRemoteRepository
constructor(
    private val api: Api
) : UserRegisterRepository {

    private val userDataMapper = UserDataEntityMapper()

    override fun register(
        name_family: String,
        email: String,
        password: String
    ): Observable<UserEntity> {
        return api.register(RegisterRequestData( name = name_family, email = email, password = password))
            .flatMap { user ->
                Observable.just(userDataMapper.mapFrom(user))
            }
    }
}