package app.parkup.data.entities

data class OrderData(
    val check_in_time: String?=null,
    val check_out_time: String?=null,
    val cost: Int?=null,
    val created_at: String?=null,
    val duration_text: String?=null,
    val id: Int?=null,
    val order_code: String?=null,
    val order_status: String?=null,
    val parking: ParkingData,
    val customer_phone: String?=null,
    val customer_name: String?=null,
    val updated_at: String?=null,
    val vehicle_plate_number: String?=null,
    val user: Int?=null,
    val vehicle: VehicleData
)