package app.parkup.data.entities

data class MessageData(
    val message: String?
)