package app.parkup.data.entities

data class LocalVehicleData (
    val id:Int,
    val name:String
)