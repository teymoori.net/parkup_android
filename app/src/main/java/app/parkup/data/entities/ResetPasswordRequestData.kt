package app.parkup.data.entities

data class ResetPasswordRequestData(
    val otp: String,
    val password: String,
    val hash: String
)