package app.parkup.data.entities

data class StripeSecretData(
    val client_secret: String?,
    val email: String?,
    val phone_number: String?,
    val name: String?,
    val publishable_key: String?
)