package app.parkup.data.entities

data class PaymentData(
    val amount: Int?,
    val canceled_at: String?,
    val cancellation_reason: String?,
    val capture_method: String?,
    val id: String?,
    val status: String?
)