package app.parkup.data.entities

data class ForgetPasswordData(
    val message: String?,
    val hash: String? = null
)