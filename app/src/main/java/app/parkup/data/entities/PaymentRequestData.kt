package app.parkup.data.entities

data class PaymentRequestData(
    val order_id: Long
)