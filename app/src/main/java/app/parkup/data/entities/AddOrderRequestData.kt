package app.parkup.data.entities

data class AddOrderRequestData(
    val vehicleID: Int?,
    val parkingID: Int?,
    val hours: Int?,
    val cost: Int?
)