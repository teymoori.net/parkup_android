package app.parkup.data.entities

data class ParkingRequestData(
    val latitude: Double?,
    val longitude: Double?
)