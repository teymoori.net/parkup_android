package app.parkup.data.entities


data class LoginRequestData(
    val username: String,
    val password: String
)
