package app.parkup.data.entities

data class TermConditionData(
    val detail: String,
    val title: String,
    val updated_at: String
)