package app.parkup.data.entities

data class APICallResponseData(
    val error: String,
    val hasError: Boolean,
    val succeed: Boolean
)