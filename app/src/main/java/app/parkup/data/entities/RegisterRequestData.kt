package app.parkup.data.entities


data class RegisterRequestData(
    val name: String,
    val email: String,
    val password: String
)