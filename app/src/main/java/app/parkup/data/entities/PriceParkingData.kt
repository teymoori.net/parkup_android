package app.parkup.data.entities


data class PriceParkingData(
    val type: String?,
    val title: String?,
    val cost: Int? ,
    val hours_expiry: Int?
)
