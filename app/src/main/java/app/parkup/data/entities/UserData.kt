package app.parkup.data.entities

data class UserData(
    val access_token: String?,
    val email: String?,
    val family: String?,
    val gender: String?,
    val id: Int,
    val name: String?,
    val phone_number:String?,
    val role_id: String?
)