package app.parkup.data.entities

data class VehicleData(
    val color: String?=null,
    val color_rgb: String?=null,
    val created_at: String?=null,
    val id: Int?=null,
    val model: String?=null,
    val plate_number: String?=null,
    val updated_at: String?=null,
    val user_owner: Int?=null,
    val default_vehicle: Boolean?=false
)