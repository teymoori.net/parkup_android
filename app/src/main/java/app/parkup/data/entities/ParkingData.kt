package app.parkup.data.entities


data class ParkingData(
    val area: String?=null,
    val created_at: String?=null,
    val daily_cost: String?=null,
    val email: String?=null,
    val full_address: String?=null,
    val id: Int?=null,
    val image: String?=null,
    val is_open: Boolean?=null,
    val latitude: Double,
    val longitude: Double,
    val manager: String?=null,
    val monthly_cost: String?=null,
    val one_hour_cost: Int?=null,
    val max_hours_accept: Int?=null,
    val phone: String?=null,
    val rank: Int?=null,
    val title: String?=null,
    val distance: Double?=null,
    val updated_at: String?=null,
    val start: String?=null,
    val end: String?=null,
    val prices: List<PriceParkingData>?=null,
    val accept_hourly: String?=null,
    val cost: Double?=null,
    val duration_text: String?=null,
    val is_attended: Boolean?=null,
    val is_covered: Boolean?=null,
    val is_valet: Boolean?=null,
    val hours_operation: String?=null,
    val description: String?=null

)
