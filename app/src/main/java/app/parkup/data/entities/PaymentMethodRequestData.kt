package app.parkup.data.entities


data class PaymentMethodRequestData(
    val method_id: String
)
