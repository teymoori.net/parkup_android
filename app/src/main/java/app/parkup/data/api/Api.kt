package app.parkup.data.api

import app.parkup.data.entities.*
import app.parkup.domain.entities.PaymentEntity
import com.google.android.gms.common.internal.safeparcel.SafeParcelable
import io.reactivex.Observable
import retrofit2.http.*

interface Api {


    @Headers("Content-Type: application/json", "Accept: application/json", "No-Authentication:null")
    @POST("register")
    fun register(
        @Body data: RegisterRequestData
    ): Observable<UserData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("login")
    fun login(
        @Body data: LoginRequestData
    ): Observable<UserData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("user")
    fun getUserDetails(): Observable<UserData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("parkings")
    fun getParkings(
        @Query("latitude") latitude: Double?,
        @Query("longitude") longitude: Double?
    ): Observable<List<ParkingData>>

    @GET("parkings/{id}")
    fun getParking(
        @Path("id") id: Int
    ): Observable<ParkingData>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("vehicles")
    fun addVehicle(
        @Body data: VehicleData
    ): Observable<VehicleData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("vehicles")
    fun getVehicles(): Observable<List<VehicleData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @DELETE("vehicles/{id}")
    fun removeVehicle(
        @Path("id") id: Int
    ): Observable<Void>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @PUT("vehicles/set_default/{id}")
    fun setDefaultVehicle(
        @Path("id") id: Int
    ): Observable<VehicleData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("get_payment_methods")
    fun getPaymentMethods(): Observable<List<PaymentMethodData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("get_secret")
    fun getStripeSecrets(): Observable<StripeSecretData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("set_default_payment_method")
    fun setDefaultPaymentMethod(
        @Body request: PaymentMethodRequestData
    ): Observable<APICallResponseData>

    @Headers("Content-Type: application/json", "Accept: application/json")
//    @DELETE("remove_payment_method")
    @HTTP(method = "DELETE", path = "remove_payment_method", hasBody = true)
    fun removePaymentMethod(
        @Body request: PaymentMethodRequestData
    ): Observable<APICallResponseData>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("orders")
    fun addOrder(
        @Body data: AddOrderRequestData
    ): Observable<OrderData>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("orders/{id}")
    fun getOrder(@Path("id") id: Int): Observable<OrderData>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("orders")
    fun getOrders(): Observable<List<OrderData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("pay")
    fun pay(
        @Body data: PaymentRequestData
    ): Observable<PaymentData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("search")
    fun searchOrder(@Query("word") word: String): Observable<List<OrderData>>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("forget_password")
    fun forgetPassword(
        @Query("username") username: String
    ): Observable<ForgetPasswordData>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("reset_password")
    fun resetPassword(
        @Body data: ResetPasswordRequestData
    ): Observable<MessageData>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("terms")
    fun getTermsConditions(): Observable<List<TermConditionData>>

}