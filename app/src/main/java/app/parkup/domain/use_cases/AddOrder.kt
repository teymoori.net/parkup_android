package app.parkup.domain.use_cases

import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

class AddOrder(
    transformer: Transformer<OrderEntity>,
    private val ordersRepository: OrdersRepository
) : UseCase<OrderEntity>(transformer) {

    companion object {
        private const val ORDER = "order"
    }

    fun addOrder(orderRequestData: AddOrderRequestData): Observable<OrderEntity> {
        val data = HashMap<String, AddOrderRequestData>()
        data[ORDER] = orderRequestData
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<OrderEntity> {
        val order = data?.get(ORDER) as? AddOrderRequestData
        order?.let {
            return ordersRepository.addNewOrder(order)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }

    }

}

