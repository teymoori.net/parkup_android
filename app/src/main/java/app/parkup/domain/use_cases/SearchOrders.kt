package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

class SearchOrders(
    transformer: Transformer<List<OrderEntity>>,
    private val ordersRepository: OrdersRepository
) : UseCase<List<OrderEntity>>(transformer) {

    companion object {
        private const val WORD = "word"
    }

    fun searchOrder(word: String): Observable<List<OrderEntity>> {
        val data = HashMap<String, String>()
        data[WORD] = word
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<List<OrderEntity>> {
        val word = data?.get(WORD) as? String
        word?.let {
            return ordersRepository.searchOrder(word)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }
    }

}

