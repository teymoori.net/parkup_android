package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.TermConditionEntitiy
import app.parkup.domain.interactors.terms.TermsConditionsRepository
import io.reactivex.Observable

class GetTermsConditions(
    transformer: Transformer<List<TermConditionEntitiy>>,
    private val termsConditionsRepository: TermsConditionsRepository
) : UseCase<List<TermConditionEntitiy>>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<List<TermConditionEntitiy>> {
        return termsConditionsRepository.getTermsConditions()
    }
}

