package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_login.UserLoginRepository
import io.reactivex.Observable

class LoginUser(
    transformer: Transformer<UserEntity>,
    private val userLoginRepository: UserLoginRepository
) : UseCase<UserEntity>(transformer) {

    companion object {
        private const val USERNAME = "username"
        private const val PASSWORD = "password"
    }

    fun login(username: String, password: String): Observable<UserEntity> {
        val data = HashMap<String, String>()
        data[USERNAME] = username
        data[PASSWORD] = password
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<UserEntity> {
        val username = data?.get(USERNAME) as? String
        val password = data?.get(PASSWORD) as? String
        return if (username.isNullOrEmpty() && password.isNullOrEmpty()) {
            Observable.error { IllegalArgumentException("Username/Password must be provided.") }
        } else {
            userLoginRepository.login(username.toString() , password.toString())
        }
    }

}

