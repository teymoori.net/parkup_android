package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class GetVehicles(
    transformer: Transformer<List<VehicleEntity>>,
    private val vehiclesRepository: VehiclesRepository
) : UseCase<List<VehicleEntity>>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<List<VehicleEntity>> {
        return vehiclesRepository.getVehicles()

    }

}

