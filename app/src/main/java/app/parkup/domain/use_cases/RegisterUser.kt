package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_register.UserRegisterRepository
import io.reactivex.Observable

class RegisterUser(
    transformer: Transformer<UserEntity>,
    private val userRegisterRepository: UserRegisterRepository
) : UseCase<UserEntity>(transformer) {

    companion object {
        private const val NAME = "name_family"
        private const val EMAIL = "email"
        private const val PASSWORD = "password"
    }

    fun register(name_family: String, email: String, password: String): Observable<UserEntity> {
        val data = HashMap<String, String>()
        data[NAME] = name_family
        data[EMAIL] = email
        data[PASSWORD] = password
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<UserEntity> {
        val nameFamily = data?.get(NAME) as? String
        val email = data?.get(EMAIL) as? String
        val password = data?.get(PASSWORD) as? String

        return if (nameFamily.isNullOrEmpty() && email.isNullOrEmpty() && password.isNullOrEmpty()) {
            Observable.error { IllegalArgumentException("NameFamily/Email/Password must be provided.") }
        } else {
            userRegisterRepository.register(
                name_family = nameFamily.toString(),
                email = email.toString(),
                password = password.toString()
            )
        }
    }

}

