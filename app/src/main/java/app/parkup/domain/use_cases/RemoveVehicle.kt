package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class RemoveVehicle(
    transformer: Transformer<Boolean>,
    private val vehiclesRepository: VehiclesRepository
) : UseCase<Boolean>(transformer) {

    companion object {
        private const val VEHICLE = "vehicle"
    }

    fun removeVehicle(vehicle: VehicleEntity): Observable<Boolean> {
        val data = HashMap<String, VehicleEntity>()
        data[VEHICLE] = vehicle
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<Boolean> {
        val vehicle = data?.get(VEHICLE) as? VehicleEntity
        vehicle?.let {
            return vehiclesRepository.removeVehicle(vehicle)
        }
        return Observable.error { IllegalArgumentException("Vehicle must be provided.") }

    }

}

