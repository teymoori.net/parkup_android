package app.parkup.domain.use_cases

import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.ForgetPasswordEntity
import app.parkup.domain.interactors.forget_password.ForgetPasswordRepository
import io.reactivex.Observable

class ForgetPassword(
    transformer: Transformer<ForgetPasswordEntity>,
    private val forgetPasswordRepository: ForgetPasswordRepository
) : UseCase<ForgetPasswordEntity>(transformer) {

    companion object {
        private const val USERNAME = "username"
    }

    fun requestCode(username: String?): Observable<ForgetPasswordEntity> {
        val data = HashMap<String, String>()
        data[USERNAME] = username ?: ""
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<ForgetPasswordEntity> {
        val username = data?.get(USERNAME) as? String
        username?.let {
            return forgetPasswordRepository.requestCode(username)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }

    }

}

