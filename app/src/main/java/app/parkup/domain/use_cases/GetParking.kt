package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.interactors.get_parkings.GetParkingsRepository
import io.reactivex.Observable

class GetParking(
    transformer: Transformer<ParkingEntity>,
    private val parkingRepository: GetParkingsRepository
) : UseCase<ParkingEntity>(transformer) {

    companion object {
        private const val PARKING_ID = "parking_id"
    }

    fun getParking(parkingId: Int): Observable<ParkingEntity> {
        val data = HashMap<String, Int>()
        data[PARKING_ID] = parkingId
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<ParkingEntity> {
        val id = data?.get(PARKING_ID) as? Int
        id?.let {
            return parkingRepository.getParking(id)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }

    }

}

