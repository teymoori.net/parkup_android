package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.interactors.payment_methods.PaymentRepository
import io.reactivex.Observable

class Pay(
    transformer: Transformer<PaymentEntity>,
    private val paymentRepository: PaymentRepository
) : UseCase<PaymentEntity>(transformer) {

    companion object {
        private const val ORDER_ID = "order_id"
    }

    fun pay(orderID:Long): Observable<PaymentEntity> {
        val data = HashMap<String, Long>()
        data[ORDER_ID] = orderID
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<PaymentEntity> {
        val orderID = data?.get(ORDER_ID) as? Long
        orderID?.let {
            return paymentRepository.pay(orderID)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }

    }

}

