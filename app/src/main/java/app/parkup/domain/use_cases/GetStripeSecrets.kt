package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.StripeSecretEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.stripe_secret.StripeSecretRepository
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class GetStripeSecrets(
    transformer: Transformer<StripeSecretEntity>,
    private val stripeSecretRepository: StripeSecretRepository
) : UseCase<StripeSecretEntity>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<StripeSecretEntity> {
        return stripeSecretRepository.getSecrets()
    }
}

