package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.interactors.user_login.UserCache
import io.reactivex.Observable

class LogOutUser(
    private val userCache: UserCache,
    transformer: Transformer<Boolean>
) : UseCase<Boolean>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<Boolean> {
        return userCache.removeUser()
    }

}

