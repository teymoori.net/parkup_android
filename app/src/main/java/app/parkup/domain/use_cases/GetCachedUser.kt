package app.parkup.domain.use_cases

import app.parkup.domain.common.Optional
import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_login.UserCache
import io.reactivex.Observable

class GetCachedUser(
    private val userCache: UserCache,
    transformer: Transformer<Optional<UserEntity>>
) : UseCase<Optional<UserEntity>>(transformer) {


    override fun createObservable(data: Map<String, Any>?): Observable<Optional<UserEntity>> {
        return userCache.getUser()
    }

}

