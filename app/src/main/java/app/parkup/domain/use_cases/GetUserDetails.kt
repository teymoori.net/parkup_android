package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.interactors.user_details.UserDetailsRepository
import io.reactivex.Observable

class GetUserDetails(
    transformer: Transformer<UserEntity>,
    private val userDetailsRepository: UserDetailsRepository
) : UseCase<UserEntity>(transformer) {


    override fun createObservable(data: Map<String, Any>?): Observable<UserEntity> {
        return userDetailsRepository.getDetails()
    }


}

