package app.parkup.domain.use_cases

import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

class GetOrder(
    transformer: Transformer<OrderEntity>,
    private val ordersRepository: OrdersRepository
) : UseCase<OrderEntity>(transformer) {

    companion object {
        private const val ORDER_ID = "order_id"
    }

    fun getOrder(orderID: Int): Observable<OrderEntity> {
        val data = HashMap<String, Int>()
        data[ORDER_ID] = orderID
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<OrderEntity> {
        val orderID = data?.get(ORDER_ID) as? Int
        orderID?.let {
            return ordersRepository.getOrder(orderID)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }

    }

}

