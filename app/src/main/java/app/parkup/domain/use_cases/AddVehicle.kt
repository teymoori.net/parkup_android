package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class AddVehicle(
    transformer: Transformer<VehicleEntity>,
    private val vehiclesRepository: VehiclesRepository
) : UseCase<VehicleEntity>(transformer) {

    companion object {
        private const val VEHICLE = "vehicle"
    }

    fun addVehicle(vehicle: VehicleEntity): Observable<VehicleEntity> {
        val data = HashMap<String, VehicleEntity>()
        data[VEHICLE] = vehicle
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<VehicleEntity> {
        val vehicle = data?.get(VEHICLE) as? VehicleEntity
        vehicle?.let {
            return vehiclesRepository.addVehicle(vehicle)
        }
        return Observable.error { IllegalArgumentException("Username/Password must be provided.") }

    }

}

