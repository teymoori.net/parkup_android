package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.APICallResponseEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.payment_methods.PaymentMethodsRepository
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import io.reactivex.Observable

class RemovePaymentMethod(
    transformer: Transformer<APICallResponseEntity>,
    private val paymentMethodsRepository: PaymentMethodsRepository
) : UseCase<APICallResponseEntity>(transformer) {

    companion object {
        private const val PAYMENT_METHOD_ID = "payment_method_id"
    }

    fun removeMethod(paymentMethod: PaymentMethodEntity ): Observable<APICallResponseEntity> {
        val data = HashMap<String, String>()
        data[PAYMENT_METHOD_ID] = paymentMethod.id
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<APICallResponseEntity> {
        val paymentMethodID = data?.get(PAYMENT_METHOD_ID) as? String
        paymentMethodID?.let {
            return paymentMethodsRepository.removeMethod(it)
        }
        return Observable.error { IllegalArgumentException("Username/Password must be provided.") }
    }

}

