package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.interactors.get_parkings.GetParkingsRepository
import app.parkup.domain.interactors.payment_methods.PaymentMethodsRepository
import io.reactivex.Observable

class GetPaymentMethods(
    transformer: Transformer<List<PaymentMethodEntity>>,
    private val paymentMethodsRepository: PaymentMethodsRepository
) : UseCase<List<PaymentMethodEntity>>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<List<PaymentMethodEntity>> {
        return paymentMethodsRepository.getMethods()
    }

}

