package app.parkup.domain.use_cases

import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.interactors.get_parkings.GetParkingsRepository
import io.reactivex.Observable

class GetParkings(
    transformer: Transformer<List<ParkingEntity>>,
    private val parkingsRepository: GetParkingsRepository
) : UseCase<List<ParkingEntity>>(transformer) {

    companion object {
        private const val REQUEST = "request"
    }

    fun getParkings(request: ParkingRequestData): Observable<List<ParkingEntity>> {
        val data = HashMap<String, ParkingRequestData>()
        data[REQUEST] = request
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<List<ParkingEntity>> {
        val request = (data?.get(REQUEST) as? ParkingRequestData)
            ?: return Observable.error { IllegalArgumentException("Data Mandatory") }
        return parkingsRepository.getParkings(request)
    }


}

