package app.parkup.domain.use_cases

import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

class GetOrders(
    transformer: Transformer<List<OrderEntity>>,
    private val ordersRepository: OrdersRepository
) : UseCase<List<OrderEntity>>(transformer) {

    override fun createObservable(data: Map<String, Any>?): Observable<List<OrderEntity>> {
        return ordersRepository.getOrders()

    }

}

