package app.parkup.domain.use_cases

import app.parkup.data.entities.ResetPasswordRequestData
import app.parkup.domain.common.Transformer
import app.parkup.domain.entities.ForgetPasswordEntity
import app.parkup.domain.entities.MessageEntity
import app.parkup.domain.interactors.forget_password.ForgetPasswordRepository
import io.reactivex.Observable

class ResetPassword(
    transformer: Transformer<MessageEntity>,
    private val forgetPasswordRepository: ForgetPasswordRepository
) : UseCase<MessageEntity>(transformer) {

    companion object {
        private const val DATA = "data"
    }

    fun resetPassword(request: ResetPasswordRequestData): Observable<MessageEntity> {
        val data = HashMap<String, ResetPasswordRequestData>()
        data[DATA] = request
        return observable(data)
    }

    override fun createObservable(data: Map<String, Any>?): Observable<MessageEntity> {
        val request = data?.get(DATA) as? ResetPasswordRequestData
        request?.let {
            return forgetPasswordRepository.resetPassword(request)
        }
        return Observable.error { IllegalArgumentException("Fields are Mandatory.") }

    }

}

