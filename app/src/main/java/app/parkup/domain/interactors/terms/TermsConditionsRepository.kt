package app.parkup.domain.interactors.terms

import app.parkup.domain.entities.TermConditionEntitiy
import io.reactivex.Observable

interface TermsConditionsRepository {
    fun getTermsConditions(): Observable<List<TermConditionEntitiy>>
}