package app.parkup.domain.interactors.user_details

import app.parkup.domain.entities.UserEntity
import io.reactivex.Observable

interface UserDetailsRepository {
    fun getDetails(  ): Observable<UserEntity>
}