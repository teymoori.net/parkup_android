package app.parkup.domain.interactors.get_parkings

import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.entities.ParkingEntity
import io.reactivex.Observable

interface GetParkingsRepository {
    fun getParkings(request: ParkingRequestData): Observable<List<ParkingEntity>>
    fun getParking(id:Int): Observable<ParkingEntity>
}
