package app.parkup.domain.interactors.order

import app.parkup.data.entities.AddOrderRequestData
import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

interface OrdersRepository {
    fun getOrder(id: Int): Observable<OrderEntity>
    fun getOrders(): Observable<List<OrderEntity>>
    fun searchOrder(word:String): Observable<List<OrderEntity>>
    fun addNewOrder(request: AddOrderRequestData): Observable<OrderEntity>
    fun payOrder(orderID: Long): Observable<OrderEntity>
}
