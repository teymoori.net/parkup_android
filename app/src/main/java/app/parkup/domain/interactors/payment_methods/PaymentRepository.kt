package app.parkup.domain.interactors.payment_methods

import app.parkup.data.entities.AddOrderRequestData
import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.PaymentEntity
import app.parkup.utils.tools.OrderEntity
import io.reactivex.Observable

interface PaymentRepository {
    fun pay(orderID: Long): Observable<PaymentEntity>
}
