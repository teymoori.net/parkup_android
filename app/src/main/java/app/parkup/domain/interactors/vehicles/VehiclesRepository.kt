package app.parkup.domain.interactors.vehicles

import app.parkup.domain.entities.VehicleEntity
import io.reactivex.Observable

interface VehiclesRepository {
    fun getVehicles(): Observable<List<VehicleEntity>>
    fun addVehicle(vehicle: VehicleEntity): Observable<VehicleEntity>
    fun removeVehicle(vehicle: VehicleEntity): Observable<Boolean>
    fun setDefaultVehicle(id: Int): Observable<VehicleEntity>
}
