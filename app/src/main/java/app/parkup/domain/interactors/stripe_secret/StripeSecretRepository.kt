package app.parkup.domain.interactors.stripe_secret

import app.parkup.domain.entities.StripeSecretEntity
import io.reactivex.Observable

interface StripeSecretRepository {
    fun getSecrets(): Observable<StripeSecretEntity>
}
