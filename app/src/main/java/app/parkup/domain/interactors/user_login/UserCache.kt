package app.parkup.domain.interactors.user_login

import app.parkup.domain.common.Optional
import app.parkup.domain.entities.UserEntity
import io.reactivex.Observable

interface UserCache {
    fun isUserLoggedIn(): Observable<Boolean>
    fun setUser(user:UserEntity): Observable<Boolean>
    fun getUser():Observable<Optional<UserEntity>>
    fun removeUser(): Observable<Boolean>
}