package app.parkup.domain.interactors.user_login

import app.parkup.domain.entities.UserEntity
import io.reactivex.Observable

interface UserLoginRepository {
    fun login(username: String, password: String): Observable<UserEntity>
}
