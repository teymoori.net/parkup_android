package app.parkup.domain.interactors.payment_methods

import app.parkup.domain.entities.APICallResponseEntity
import app.parkup.domain.entities.PaymentMethodEntity
import io.reactivex.Observable

interface PaymentMethodsRepository {
    fun getMethods(): Observable<List<PaymentMethodEntity>>
    fun setDefaultMethod(paymentMethodID:String) : Observable<APICallResponseEntity>
    fun removeMethod(paymentMethodID:String) : Observable<APICallResponseEntity>
}
