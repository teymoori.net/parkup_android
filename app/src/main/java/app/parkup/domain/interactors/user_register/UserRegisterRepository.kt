package app.parkup.domain.interactors.user_register

import app.parkup.domain.entities.UserEntity
import io.reactivex.Observable

interface UserRegisterRepository {
    fun register(name_family: String, email: String, password: String): Observable<UserEntity>
}
