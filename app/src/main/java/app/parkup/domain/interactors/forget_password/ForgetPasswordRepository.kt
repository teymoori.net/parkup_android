package app.parkup.domain.interactors.forget_password

import app.parkup.data.entities.ResetPasswordRequestData
import app.parkup.domain.entities.ForgetPasswordEntity
import app.parkup.domain.entities.MessageEntity
import app.parkup.domain.entities.ParkingEntity
import io.reactivex.Observable

interface ForgetPasswordRepository {
    fun requestCode(username: String): Observable<ForgetPasswordEntity>
    fun resetPassword(request: ResetPasswordRequestData): Observable<MessageEntity>
}
