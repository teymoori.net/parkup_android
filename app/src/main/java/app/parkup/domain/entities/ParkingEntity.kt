package app.parkup.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParkingEntity(
    val area: String?,
    val created_at: String?,
    val daily_cost: String?,
    val email: String?,
    val full_address: String?,
    val id: Int?,
    val image: String?,
    val is_open: Boolean?,
    val latitude: Double,
    val longitude: Double,
    val manager: String?,
    val monthly_cost: String?,
    val one_hour_cost: Int?,
    val max_hours_accept: Int?,
    val phone: String?,
    val rank: Int?,
    val title: String?,
    val distance: Double?,
    val updated_at: String?,
    val start: String?,
    val end: String?,
    val accept_hourly: String?,
    val cost: Double?,
    val prices: List<PriceParkingEntity>?,
    val duration_text: String?,
    val is_attended: Boolean?,
    val is_covered: Boolean?,
    val is_valet: Boolean?,
    val hours_operation: String?,
    val description: String?
) : Parcelable

