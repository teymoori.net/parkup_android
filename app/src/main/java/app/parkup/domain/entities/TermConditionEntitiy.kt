package app.parkup.domain.entities

data class TermConditionEntitiy(
    val detail: String,
    val title: String,
    val updated_at: String
)