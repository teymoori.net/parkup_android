package app.parkup.domain.entities

data class PaymentEntity(
    val amount: Int?,
    val canceled_at: String?,
    val cancellation_reason: String?,
    val capture_method: String?,
    val id: String?,
    val status: String?
)