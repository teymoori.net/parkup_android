package app.parkup.domain.entities

data class APICallResponseEntity(
    val error: String,
    val hasError: Boolean,
    val succeed: Boolean
)