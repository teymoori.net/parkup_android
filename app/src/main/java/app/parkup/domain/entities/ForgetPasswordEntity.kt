package app.parkup.domain.entities

data class ForgetPasswordEntity(
    val message: String?,
    val hash: String? = null
)