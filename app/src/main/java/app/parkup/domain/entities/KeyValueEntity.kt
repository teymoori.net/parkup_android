package app.parkup.domain.entities

data class KeyValueEntity(
    val key: String?,
    val value: String?
)