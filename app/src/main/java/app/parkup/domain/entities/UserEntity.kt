package app.parkup.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserEntity(
    val access_token: String?,
    val email: String?,
    val family: String?,
    val gender: String?,
    val id: Int,
    val name: String?,
    val phone_number:String?,
    val role_id: String?
) : Parcelable