package app.parkup.domain.entities

data class PaymentMethodEntity(
    val brand: String,
    val exp_month: Int,
    val exp_year: Int,
    val fingerprint: String,
    val funding: String,
    val id: String,
    val last4: String,
    val name: String,
    val is_default: Boolean
)