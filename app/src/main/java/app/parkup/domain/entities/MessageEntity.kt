package app.parkup.domain.entities

data class MessageEntity(
    val message: String?
)