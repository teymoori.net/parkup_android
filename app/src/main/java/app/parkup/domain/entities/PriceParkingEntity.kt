package app.parkup.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PriceParkingEntity(
    val type: String?,
    val title: String?,
    val cost: Int?,
    val hours_expiry: Int?
) : Parcelable
