package app.parkup.presentation.ui.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_splash.*
import javax.inject.Inject

class SplashFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(SplashViewModel::class.java)

        viewModel.userLoggedIn.observe(viewLifecycleOwner, Observer { userLoggedIn ->
            if (userLoggedIn) {
                Navigation.findNavController(container).navigate(
                    R.id.action_splashFragment_to_dashboardFragment
                )
            } else {
                Navigation.findNavController(container).navigate(
                    R.id.action_splashFragment_to_loginFragment
                )
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

}
