package app.parkup.presentation.ui.login

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.use_cases.AddVehicle
import app.parkup.domain.use_cases.GetCachedUser
import app.parkup.domain.use_cases.LoginUser
import com.onesignal.OneSignal
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val loginUser: LoginUser

) : BaseViewModel() {

    val loginViewState: MutableLiveData<Response<UserEntity>> = MutableLiveData()

    fun login(username: String?, password: String?) {
        if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            loginViewState.value = ErrorIn("Username/Password is empty.")
        } else {
            loginViewState.value = Loading(true)
            compositeDisposable.add(

                loginUser.login(username, password).subscribe({
                    OneSignal.setSubscription(true)
                    OneSignal.sendTag("USER_EMAIL", it.email)

                    loginViewState.value = Success(it)
//                    loginViewState.value =
//                        loginViewState.value?.copy(showLoading = false, data = it)
                },
                    {
                        loginViewState.value = ErrorIn(it.message)
                        //  loginViewState.value = loginViewState.value?.copy(showLoading = false)
                    })

            )

        }

    }

}