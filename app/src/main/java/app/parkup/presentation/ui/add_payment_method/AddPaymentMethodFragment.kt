package app.parkup.presentation.ui.add_payment_method

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.domain.entities.StripeSecretEntity
import app.parkup.presentation.ui.scan_card.CardDetailsEntity
import app.parkup.utils.tools.Cons
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.log
import com.stripe.android.ApiResultCallback
import com.stripe.android.SetupIntentResult
import com.stripe.android.Stripe
import com.stripe.android.model.ConfirmSetupIntentParams
import com.stripe.android.model.PaymentMethod
import com.stripe.android.model.PaymentMethodCreateParams
import com.stripe.android.model.StripeIntent
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_add_payment_method.*
import javax.inject.Inject

class AddPaymentMethodFragment : BaseFragment() {

    private lateinit var stripe: Stripe

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AddPaymentMethodsViewModel

    var canSkip = false
    lateinit var cardDetails: CardDetailsEntity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            canSkip = it.getBoolean(Cons.CAN_SKIP, false)
            if (it.containsKey(Cons.ITEM_BUNDLE))
                cardDetails = it.getParcelable(Cons.ITEM_BUNDLE)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_payment_method, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddPaymentMethodsViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }
        pageTitle.text = getString(R.string.add_pm_method)


        add.setOnClickListener {
            viewModel.getStripeDetails()
        }

        viewModel.getStripeDetails.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    addPaymentMethod(it.data)
                }
                is Loading -> {
                    add.showLoading(true)
                    loading(true)
                }
                is ErrorIn -> {
                    add.showLoading(false)
                    loading(false)
                    showError(true, it.message)
                }
            }
        })
        skip.visibility = if (canSkip) View.VISIBLE else View.GONE
        skip.setOnClickListener {
            goToDashboard()
        }


        scanCard.setOnClickListener {
            checkCameraPermission()
        }

    }

    private fun goToDashboard() {
        Navigation.findNavController(skip).navigate(
            R.id.action_paymentMethodsFragment_to_dashboardFragment
        )
    }

    private fun addPaymentMethod(stripeData: StripeSecretEntity?) {
        stripe = Stripe(activity as Context, stripeData?.publishable_key ?: "")
        val paymentMethodCard = cardInputWidget.paymentMethodCard

        val billingDetails = PaymentMethod.BillingDetails.Builder()
            .setEmail(stripeData?.email)
            .setName(stripeData?.name)
            .setPhone(stripeData?.phone_number)
            .build()
        if (paymentMethodCard != null) {
            val paymentMethodParams = PaymentMethodCreateParams
                .create(paymentMethodCard, billingDetails, null)
            val confirmParams = ConfirmSetupIntentParams
                .create(paymentMethodParams, stripeData?.client_secret ?: "")
            stripe.confirmSetupIntent(this, confirmParams)
        } else {
            showError(true, "Error in Adding PaymentMethod")
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        stripe.onSetupResult(requestCode, data, object : ApiResultCallback<SetupIntentResult> {
            override fun onSuccess(result: SetupIntentResult) {
                add.showLoading(false)
                loading(false)
                val setupIntent = result.intent
                val status = setupIntent.status
                if (status == StripeIntent.Status.Succeeded) {
                    if (canSkip) {
                        goToDashboard()
                    }
                    showError(false, "Payment Method added")
                    activity?.onBackPressed()

                } else if (status == StripeIntent.Status.RequiresPaymentMethod) {
                    "failed".log("okhttp stripe_")
                    showError(true, "Error in Adding PaymentMethod")
                }
            }

            override fun onError(e: Exception) {
                add.showLoading(false)
                loading(false)
                showError(true, e.message)
                "$e onError".log("stripe_")
            }
        })
    }

    private fun checkCameraPermission() {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    Navigation.findNavController(scanCard).navigate(
                        R.id.action_addPaymentMethodFragment_to_cardScannerFragment
                    )
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                }
            }).check()
    }

    override fun onResume() {
        super.onResume()

        if (::cardDetails.isInitialized) {
            cardInputWidget.setCardNumber(cardDetails.cardNumber)
            cardInputWidget.setExpiryDate(
                cardDetails.month?.toInt() ?: 0,
                cardDetails.year?.toInt() ?: 0
            )
        }

    }
}