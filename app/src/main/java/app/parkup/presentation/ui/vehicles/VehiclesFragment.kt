package app.parkup.presentation.ui.vehicles

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.domain.entities.VehicleEntity
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.toast
import com.pixabay.utils.views.BaseDialog
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_vehicles.*
import javax.inject.Inject

class VehiclesFragment : BaseFragment() ,
    VehiclesVerticalAdapter.ClickListener {

    private lateinit var removeDialog: BaseDialog

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: VehiclesViewModel

    @Inject
    lateinit var vehiclesAdapter: VehiclesVerticalAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_vehicles, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(VehiclesViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }
        pageTitle.text = getString(R.string.vehicles)

        vehicles.adapter = vehiclesAdapter.also { it.onVehicleClickListener = this }

        viewModel.vehiclesState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    vehiclesAdapter.items = it.data
                }
                is Loading -> {
                }
                is ErrorIn -> {
                }
            }
        })
        addVehicle.setOnClickListener {
            Navigation.findNavController(pageTitle).navigate(
                R.id.action_vehiclesFragment_to_addVehicleFragment
            )
        }

    }

    private fun onLoading(show: Boolean) {
        loading(show)
    }

    private fun onError(msg: String) {
        msg.toast(activity)
    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

    override fun onVehicleRemoveClick(item: VehicleEntity?) {
        removeDialog = BaseDialog(
            activity as Activity,
            getString(R.string.sure_remove_vehicle), "No", "Yes"
        ).setRightClickEvent {
            viewModel.removeVehicle(item)
            removeDialog.dismiss()
            removeDialog.dialog.dismiss()
        }.also {
            it.show()
        }
    }

    override fun onVehicleClick(item: VehicleEntity?) {
        viewModel.setDefaultVehicle(item)
    }

}
