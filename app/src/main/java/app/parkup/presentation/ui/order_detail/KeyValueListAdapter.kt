package app.parkup.presentation.ui.order_detail

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.parkup.R
import app.parkup.domain.entities.KeyValueEntity
import app.parkup.domain.entities.PriceParkingEntity
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.tools.TimeUtils.Companion.dateTimeStringToDate
import com.pixabay.utils.tools.listen
import kotlinx.android.synthetic.main.orders_item.view.*
import javax.inject.Inject

class KeyValueListAdapter @Inject constructor() :
    BaseAdapter() {
    var items: List<KeyValueEntity>? = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private class Holder(view: View) {
        var key: TextView = view.findViewById(R.id.key)
        var value: TextView = view.findViewById(R.id.value)
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, cv: View?, parent: ViewGroup?): View {
        val viewHolder: Holder
        var convertView = cv
        if (convertView == null) {
            convertView =
                LayoutInflater.from(parent?.context)
                    .inflate(R.layout.key_value_list_item, parent, false)
            viewHolder = Holder(convertView)
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as Holder
        }

        val item = items?.get(position)
        viewHolder.key.text = item?.key
        viewHolder.value.text =item?.value

        return convertView!!

    }

    override fun getItem(position: Int): KeyValueEntity? {
        return items?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.size ?: 0
    }


}