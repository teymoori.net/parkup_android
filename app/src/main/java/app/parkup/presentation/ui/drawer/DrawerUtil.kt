package app.parkup.presentation.ui.drawer

import app.parkup.R

class DrawerUtil {
    companion object {
        fun getDrawerItems(): List<DrawerItemEntity> {
            return listOf(
                DrawerItemEntity(
                    title = "Payment Methods",
                    type = DrawerItemType.PAYMENT_METHODS,
                    icon = R.drawable.ic_payment
                ),
                DrawerItemEntity(
                    title = "History",
                    type = DrawerItemType.HISTORY,
                    icon = R.drawable.ic_history
                ),
                DrawerItemEntity(
                    title = "Vehicles",
                    type = DrawerItemType.VEHICLES,
                    icon = R.drawable.ic_car
                ),
                DrawerItemEntity(
                    title = "Settings",
                    type = DrawerItemType.SETTING,
                    icon = R.drawable.ic_settings
                ),
                DrawerItemEntity(
                    title = "Terms of Service",
                    type = DrawerItemType.TERMS,
                    icon = R.drawable.ic_sign
                ),
//                DrawerItemEntity(
//                    title = "Help",
//                    type = DrawerItemType.HELP,
//                    icon = R.drawable.ic_help
//                ),
                DrawerItemEntity(
                    title = "Log Out",
                    type = DrawerItemType.LOGOUT,
                    icon = R.drawable.ic_logout
                )
            )
        }
    }
}
