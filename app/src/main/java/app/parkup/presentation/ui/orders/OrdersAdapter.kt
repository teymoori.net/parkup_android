package app.parkup.presentation.ui.orders

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.parkup.R
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.tools.TimeUtils.Companion.dateTimeStringToDate
import javax.inject.Inject

class OrdersAdapter @Inject constructor() :
    BaseAdapter() {
    var items: List<OrderEntity>? = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    interface ClickListener {
        fun onOrderClicked(item: OrderEntity?)
    }

    private class Holder(view: View) {
        var cost: TextView = view.findViewById(R.id.cost)
        var vehicle: TextView = view.findViewById(R.id.vehicle)
        var dateTime: TextView = view.findViewById(R.id.dateTime)
        var status: TextView = view.findViewById(R.id.status)
        var parking: TextView = view.findViewById(R.id.parking)
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, cv: View?, parent: ViewGroup?): View {
        val viewHolder: Holder
        var convertView = cv
        if (convertView == null) {
            convertView =
                LayoutInflater.from(parent?.context).inflate(R.layout.orders_item, parent, false)
            viewHolder = Holder(convertView)
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as Holder
        }

        val item = items?.get(position)
        with(viewHolder) {
            cost.text = "$${item?.cost}"
            vehicle.text = "${item?.vehicle?.model} ${item?.vehicle?.color} #${item?.vehicle_plate_number}"


            dateTime.text = "${dateTimeStringToDate(item?.check_in_time)} To ${dateTimeStringToDate(item?.check_out_time)}\n( ${item?.duration_text} )"
            parking.text = "${item?.parking?.title}"
            status.text = "${item?.order_status?.capitalize()}"

            if (item?.order_status == "done" || item?.order_status == "paid")
                status.setTextColor(ContextCompat.getColor(parent?.context!!, R.color.md_green_600))
            else
                status.setTextColor(
                    ContextCompat.getColor(
                        parent?.context!!,
                        R.color.md_deep_orange_600
                    )
                )
        }

        return convertView!!
    }

    override fun getItem(position: Int): OrderEntity? {
        return items?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.size ?: 0
    }

}