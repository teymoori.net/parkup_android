package app.parkup.presentation.ui.terms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.parkup.R
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_terms_and_conditions.*
import javax.inject.Inject

class TermsAndConditionsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TermsConditionsViewModel

    @Inject
    lateinit var adapter: TermsConditionsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_terms_and_conditions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(TermsConditionsViewModel::class.java)

        pageTitle.text = getString(R.string.terms_and_conditions)
        back?.setOnClickListener { activity?.onBackPressed() }

        termsListView.adapter = adapter

        viewModel.getTerms()

        viewModel.termsResult.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    adapter.items = it.data
                    loading(false)
                }
                is Loading -> loading(true)
                is ErrorIn -> {
                    loading(false)
                    showError(isError = true, msg = it.message)
                }
            }
        })
    }
}