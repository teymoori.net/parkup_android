package app.parkup.presentation.ui.parking_detail

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import app.parkup.R
import app.parkup.domain.entities.VehicleEntity
import app.parkup.utils.tools.ImageLoader
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.tools.listen
import kotlinx.android.synthetic.main.vehicle_horizontal_list_item.view.*
import javax.inject.Inject

class VehiclesHorizontalAdapter @Inject constructor() :
    RecyclerView.Adapter<VehiclesHorizontalAdapter.Holder>() {
    private lateinit var ctx: Context
    lateinit var onItemClickListener: ClickListener
    var hasSelected = false

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        ctx = parent.context
        return Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.vehicle_horizontal_list_item,
                parent,
                false
            )
        ).listen { pos, _ ->
            items?.get(pos).let { onItemClickListener.onVehicleClicked(it) }
        }
    }

    var items: List<VehicleEntity>? = listOf()
        set(value) {
            hasSelected = value?.onEach { it.selected }.isNullOrEmpty()
            field = value
            notifyDataSetChanged()

        }

    interface ClickListener {
        fun onVehicleClicked(item: VehicleEntity?)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items?.get(position)
        holder.model.text = item?.model
        holder.plateNumber.text = item?.plate_number

        if (item?.id == -1) {
            holder.container.setBackgroundColor(ContextCompat.getColor(ctx, R.color.md_white_1000))
            holder.addContainer.visibility = View.VISIBLE
            holder.blur.visibility = View.GONE
            holder.tick.visibility = View.GONE
        } else {
            val bg = Color.parseColor(item?.color_rgb)
            holder.container.setBackgroundColor(bg)

            if (AppUtils.isColorDark(bg)) {
                holder.model.setTextColor(ContextCompat.getColor(ctx, R.color.md_blue_grey_100))
                holder.plateNumber.setTextColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.md_blue_grey_100
                    )
                )
            } else {
                holder.model.setTextColor(ContextCompat.getColor(ctx, R.color.md_blue_grey_900))
                holder.plateNumber.setTextColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.md_blue_grey_900
                    )
                )
            }

            holder.addContainer.visibility = View.GONE
        }

        if (hasSelected) {
            if (item?.selected == false) {
                holder.container.setBackgroundColor(
                    ContextCompat.getColor(
                        ctx,
                        R.color.md_white_1000
                    )
                )
                holder.addContainer.alpha = 0.5f
            } else {
                holder.addContainer.alpha = 1f
            }
        }

        if (item?.id != -1) {
            if (item?.selected == false) {

                holder.blur.visibility = View.VISIBLE
                holder.tick.visibility = View.GONE

            } else {
                holder.blur.visibility = View.GONE
                holder.tick.visibility = View.VISIBLE
            }
        }


    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val plateNumber: TextView = view.plateNumber
        val model: TextView = view.model
        val container: View = view.container
        val addContainer: View = view.addContainer
        val tick: View = view.tick
        val blur: View = view.blur
    }

}