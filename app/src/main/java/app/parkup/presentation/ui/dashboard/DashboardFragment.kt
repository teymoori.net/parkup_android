package app.parkup.presentation.ui.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.utils.tools.BaseMapFragment
import app.parkup.utils.tools.Cons
import com.github.florent37.rxgps.RxGps
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.pixabay.utils.tools.setStyle
import com.pixabay.utils.tools.toast
import com.pixabay.utils.tools.zoomToLocation
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_dashboard.*
import javax.inject.Inject


class DashboardFragment : BaseMapFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnCameraIdleListener {

    private var map: GoogleMap? = null
    var compositeDisposable = CompositeDisposable()

    var myLocationMarker: Marker? = null

    var location = LatLng(34.071212, -118.263838)

    val parkings: HashMap<Int?, ParkingEntity> = HashMap()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: DashboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        iconGen = IconGenerator(activity)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DashboardViewModel::class.java)

        pageTitle.text = "arkUp"

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.mapFragment) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
        observeVM()
        menu.setOnClickListener { toggleMenu() }


        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    activity?.finish()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        search.setOnClickListener {
            Navigation.findNavController(pageTitle).navigate(
                R.id.action_dashboardFragment_to_checkVehicleFragment
            )
            //IntentIntegrator.forSupportFragment(this).initiateScan();
        }
        scanParking.setOnClickListener {
            IntentIntegrator.forSupportFragment(this).setOrientationLocked(true).initiateScan();
        }

    }

    private fun observeVM() {
        viewModel.parkingsViewState.observe(viewLifecycleOwner, Observer {
            it.forEach {
                parkings[it.id] = it
                addIcon(it)
            }

        })
    }

    private fun addIcon(parking: ParkingEntity?) {
        val icon: Bitmap = BitmapFactory.decodeResource(
            activity?.resources,
            R.drawable.ic_marker_black_parking
        )
        val height = 105
        val width = 105
        val smallMarker = Bitmap.createScaledBitmap(icon, width, height, false)

        if (parking != null) {
            val markerOptions = MarkerOptions().icon(
                BitmapDescriptorFactory.fromBitmap(
                    smallMarker
                )
            ).position(LatLng(parking.latitude, parking.longitude))
                .anchor(iconGen.anchorU, iconGen.anchorV);

            val marker = map?.addMarker(
                markerOptions
            )
            marker?.hideInfoWindow()
            marker?.tag = parking.id
            marker?.hideInfoWindow()
        }
    }

    private fun addMyLocationMarker(location: LatLng) {

        myLocationMarker?.remove()

        val icon: Bitmap = BitmapFactory.decodeResource(
            activity?.resources,
            R.drawable.ic_current_location
        )
        val height = 105
        val width = 105
        val smallMarker = Bitmap.createScaledBitmap(icon, width, height, false)
        val markerOptions = MarkerOptions().icon(
            BitmapDescriptorFactory.fromBitmap(
                smallMarker
            )
        ).position(location)
            .anchor(iconGen.anchorU, iconGen.anchorV);
        myLocationMarker = map?.addMarker(
            markerOptions
        )
        myLocationMarker?.hideInfoWindow()
        myLocationMarker?.tag = "myLocation"
        myLocationMarker?.hideInfoWindow()

    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(mMap: GoogleMap) {
        map = mMap.apply {
//            uiSettings.isMyLocationButtonEnabled = true
            uiSettings.isZoomGesturesEnabled = true
//            uiSettings.isZoomControlsEnabled = true
            uiSettings.isCompassEnabled = true
            uiSettings.isMapToolbarEnabled = false
            setStyle(mActivity, R.raw.map)
            isMyLocationEnabled = false
            setOnMarkerClickListener(this@DashboardFragment)
            setOnCameraIdleListener(this@DashboardFragment)
        }
        getLocation()

        myLocation.setOnClickListener { getLocation() }
    }

    private fun getLocation() {
        compositeDisposable.add(
            RxGps(mActivity).lastLocation().toSingle()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ location ->
                    this.location = LatLng(location.latitude, location.longitude)
                    addMyLocationMarker(this.location)
                    map.zoomToLocation(location.latitude, location.longitude)
                    requestParkings()
                }, {
                })
        )
    }

    private fun requestParkings() {
        viewModel.getParkings(
            ParkingRequestData(
                latitude = location.latitude,
                longitude = location.longitude
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun toggleMenu() {
        if (!drawer.isDrawerOpen(GravityCompat.START))
            drawer.openDrawer(GravityCompat.START)
        else
            drawer.closeDrawer(GravityCompat.START)
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (marker?.tag != "myLocation") {
            Navigation.findNavController(pageTitle).navigate(
                R.id.action_dashboardFragment_to_parkingDetailDialogActivity,
                bundleOf(Cons.ITEM_BUNDLE to parkings[marker?.tag])
            )
        }
        return false
    }

    override fun onCameraIdle() {
        if (map != null) {
            location = map?.cameraPosition?.target ?: location
            requestParkings()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result: IntentResult =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        val barcode: String? = result.contents
        Navigation.findNavController(pageTitle).navigate(
            Uri.parse(barcode)
        )

    }

}
