package app.parkup.presentation.ui.forget_password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.domain.entities.ForgetPasswordEntity
import app.parkup.utils.tools.Cons
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_forget_password.*
import javax.inject.Inject

class ForgetPasswordFragment : BaseFragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ForgetPasswordViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_forget_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ForgetPasswordViewModel::class.java)

        //back?.setOnClickListener { activity?.onBackPressed() }

        requestOTP.setOnClickListener {
            viewModel.requestCode(username.text.toString())
        }
        viewModel.requestCode.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> showData(it.data)
                is Loading -> requestOTP.showLoading(true)
                is ErrorIn -> showError(msg = it.message)
            }
        })
    }

    private fun showData(data: ForgetPasswordEntity?) {
        requestOTP.showLoading(false)
        Navigation.findNavController(requestOTP).navigate(
            R.id.action_forgetPasswordFragment_to_resetPasswordFragment
            ,
            bundleOf(Cons.ITEM_BUNDLE to data?.hash)
        )
    }
}