package app.parkup.presentation.ui.check_vehicle

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.use_cases.SearchOrders
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import javax.inject.Inject

class CheckVehicleViewModel @Inject constructor(
    private val searchOrders: SearchOrders

) : BaseViewModel() {

    val searchState: MutableLiveData<Response<List<OrderEntity>>> = MutableLiveData()

    fun search(word: String) {
        searchState.postValue(Loading(true))
        compositeDisposable.add(

            searchOrders.searchOrder(word).subscribe({
                searchState.postValue(Success(it))
            },
                {
                    searchState.postValue(ErrorIn(it.message))
                })
        )
    }

}