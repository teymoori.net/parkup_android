package app.parkup.presentation.ui.terms

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.TermConditionEntitiy
import app.parkup.domain.use_cases.GetTermsConditions
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import javax.inject.Inject

class TermsConditionsViewModel @Inject constructor(
    private val getTermsConditions: GetTermsConditions

) : BaseViewModel() {

    val termsResult: MutableLiveData<Response<List<TermConditionEntitiy>>> = MutableLiveData()

    fun getTerms() {
        termsResult.value = Loading(true)
        compositeDisposable.add(
            getTermsConditions.observable().subscribe({
                termsResult.value = Success(it)
            },
                {
                    termsResult.value = ErrorIn(it.message)
                })

        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}