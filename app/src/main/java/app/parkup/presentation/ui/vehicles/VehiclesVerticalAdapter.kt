package app.parkup.presentation.ui.vehicles

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.parkup.R
import app.parkup.domain.entities.VehicleEntity
import kotlinx.android.synthetic.main.vehicle_horizontal_list_item.view.model
import kotlinx.android.synthetic.main.vehicle_horizontal_list_item.view.plateNumber
import kotlinx.android.synthetic.main.vehicle_vertical_list_item.view.*
import javax.inject.Inject

class VehiclesVerticalAdapter @Inject constructor() :
    RecyclerView.Adapter<VehiclesVerticalAdapter.Holder>() {
    private lateinit var ctx: Context
    lateinit var onVehicleClickListener: ClickListener
    var hasSelected = false

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        ctx = parent.context
        val holder = Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.vehicle_vertical_list_item,
                parent,
                false
            )
        )
        holder.remove.setOnClickListener {
            onVehicleClickListener.onVehicleRemoveClick(items?.get(holder.adapterPosition))
        }
        holder.container.setOnClickListener {
            onVehicleClickListener.onVehicleClick(items?.get(holder.adapterPosition))
        }

        return holder
    }

    var items: List<VehicleEntity>? = listOf()
        set(value) {
            hasSelected = value?.onEach { it.selected }.isNullOrEmpty()
            field = value
            notifyDataSetChanged()

        }


    interface ClickListener {
        fun onVehicleRemoveClick(item: VehicleEntity?)
        fun onVehicleClick(item: VehicleEntity?)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items?.get(position)
        holder.model.text = item?.model
        holder.plateNumber.text = item?.plate_number
        holder.plateNumber.setBackgroundColor(Color.parseColor(item?.color_rgb))

        if (item?.default_vehicle == true) {
            holder.isDefault.visibility = View.VISIBLE
        } else {
            holder.isDefault.visibility = View.INVISIBLE
        }

    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val container: View = view.container
        val plateNumber: TextView = view.plateNumber
        val model: TextView = view.model
        val remove: View = view.remove
        val isDefault: View = view.isDefault
    }

}