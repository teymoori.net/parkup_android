package app.parkup.presentation.ui.parking_details_dialog

import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import app.parkup.R
import app.parkup.domain.entities.ParkingEntity
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.ImageLoader
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.views.ExpandedBottomSheetDialog
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_parking_detail_dialog.*
import javax.inject.Inject


class ParkingDetailDialogFragment : ExpandedBottomSheetDialog() {

    lateinit var parking: ParkingEntity

    @Inject
    lateinit var imageLoader: ImageLoader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            parking = it.getParcelable(Cons.ITEM_BUNDLE)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_parking_detail_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onViewCreated(view, savedInstanceState)
        parkingName.text = parking.title
        parkingArea.text = parking.area
        prices.text = "$${parking.daily_cost} Daily , $${parking.monthly_cost} Monthly"
        distance.text = "${parking.distance}"
        rateValue.text = "${parking.rank}"

        rating.rating = parking.rank?.toFloat() ?: 3f
        imageLoader.load(
            url = parking.image,
            imageView = image
        )

        container.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(
                Uri.parse(AppUtils.getParkingDeepLinkUri(parkingId = parking?.id))
//                R.id.action_parkingDetailDialogActivity_to_checkOrderFragment,
//                bundleOf(Cons.ITEM_BUNDLE to parking.id)
            )
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (view?.parent as View).setBackgroundColor(Color.TRANSPARENT)
        val resources = resources

        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            val parent = view?.parent as View
            val layoutParams = parent.layoutParams as CoordinatorLayout.LayoutParams
            layoutParams.setMargins(
                resources.getDimensionPixelSize(R.dimen._12ssp), // LEFT 16dp
                0,
                resources.getDimensionPixelSize(R.dimen._12ssp), // RIGHT 16dp
                resources.getDimensionPixelSize(R.dimen._90ssp)
            )
            parent.layoutParams = layoutParams
        }
    }

}
