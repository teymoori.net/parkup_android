package app.parkup.presentation.ui.terms

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import app.parkup.R
import app.parkup.domain.entities.TermConditionEntitiy
import com.pixabay.utils.tools.log
import javax.inject.Inject

class TermsConditionsListAdapter @Inject constructor() :
    BaseAdapter() {
    var items: List<TermConditionEntitiy>? = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private class Holder(view: View) {
        var title: TextView = view.findViewById(R.id.title)
        var detail: TextView = view.findViewById(R.id.detail)
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, cv: View?, parent: ViewGroup?): View {
        val viewHolder: Holder
        var convertView = cv
        if (convertView == null) {
            convertView =
                LayoutInflater.from(parent?.context)
                    .inflate(R.layout.terms_conditions_item, parent, false)
            viewHolder = Holder(convertView)
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as Holder
        }

        val item = items?.get(position)
        viewHolder.title.text = item?.title
        viewHolder.detail.text = item?.detail

        return convertView!!

    }

    override fun getItem(position: Int): TermConditionEntitiy? {
        return items?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.size ?: 0
    }


}