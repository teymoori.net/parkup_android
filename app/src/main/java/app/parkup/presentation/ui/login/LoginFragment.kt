package app.parkup.presentation.ui.login

import android.Manifest
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.toast
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

class LoginFragment : BaseFragment(), PermissionListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        checkPermission()

        observeVM()
        //test
        username.setText("mt1368@gmail.com")
        password.setText("123456")

        login.setOnClickListener {
            viewModel.login(username.text.toString(), password.text.toString())
        }
        register.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_loginFragment_to_registerTermsConditionsFragment
            )
        }
        forgetPassword.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_loginFragment_to_forgetPasswordFragment
            )
        }
    }

    private fun observeVM() {
        viewModel.loginViewState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Loading -> loading(true)
                is Success -> success()
                is ErrorIn -> showError(true, "Email/Password is wrong")
            }
        })

    }


    private fun success() {
        loading(false)
        Navigation.findNavController(login).navigate(
            R.id.action_loginFragment_to_dashboardFragment
        )
    }

    private fun checkPermission() {
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(this).check()
    }

    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
    }

    override fun onPermissionRationaleShouldBeShown(
        permission: PermissionRequest?,
        token: PermissionToken?
    ) {

    }

    override fun onPermissionDenied(response: PermissionDeniedResponse?) {

    }


}
