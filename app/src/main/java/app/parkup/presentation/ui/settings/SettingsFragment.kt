package app.parkup.presentation.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.parkup.R
import app.parkup.domain.entities.UserEntity
import app.parkup.presentation.ui.orders.OrdersAdapter
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_settings.*
import javax.inject.Inject

class SettingsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SettingsViewModel

    @Inject
    lateinit var ordersAdapter: OrdersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(SettingsViewModel::class.java)

        pageTitle.text = getString(R.string.setting)
        back.setOnClickListener { activity?.onBackPressed() }

        viewModel.userState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> showData(it.data)
                is Loading -> loading(it.isLoading)
                is ErrorIn -> showError(true, it.message)
            }
        })
    }

    private fun showData(user: UserEntity?) {
        loading(false)
        nameFamily.setText(user?.name)
        phone.setText(user?.phone_number)
        email.setText(user?.email)
    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

}