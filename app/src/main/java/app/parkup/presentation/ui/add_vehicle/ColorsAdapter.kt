package app.parkup.presentation.ui.add_vehicle

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.parkup.R
import app.parkup.domain.entities.VehicleEntity
import app.parkup.utils.tools.ColorEntity
import app.parkup.utils.tools.ImageLoader
import com.pixabay.utils.tools.listen
import kotlinx.android.synthetic.main.color_item.view.*
import javax.inject.Inject

class ColorsAdapter @Inject constructor() :
    RecyclerView.Adapter<ColorsAdapter.Holder>() {
    private lateinit var ctx: Context
    lateinit var onItemClickListener: ClickListener
    var hasSelected = false

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        ctx = parent.context
        return Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.color_item,
                parent,
                false
            )
        ).listen { pos, _ ->
            items?.get(pos).let { onItemClickListener.onColorClicked(it) }
        }
    }

    var items: List<ColorEntity>? = listOf()
        set(value) {
            hasSelected = value?.onEach { it.selected }.isNullOrEmpty()
            field = value
            notifyDataSetChanged()

        }

    interface ClickListener {
        fun onColorClicked(item: ColorEntity?)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items?.get(position)
        val color = item?.rgb
        holder.bg.setBackgroundColor(Color.parseColor(color))

        if (item?.selected == false) {
            holder.tick.visibility = View.GONE
        } else {
            holder.tick.visibility = View.VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val bg: View = view.bg
        val tick: View = view.tick
    }

}