package app.parkup.presentation.ui.payment_methods

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.parkup.R
import app.parkup.domain.entities.PaymentMethodEntity
import com.pixabay.utils.tools.listen
import com.pixabay.utils.views.MyTextView
import kotlinx.android.synthetic.main.payment_methods_list_item.view.*
import javax.inject.Inject

class PaymentMethodsAdapter @Inject constructor() :
    RecyclerView.Adapter<PaymentMethodsAdapter.Holder>() {
    private lateinit var ctx: Context
    lateinit var onItemClickListener: ClickListener

    var showRemoveButton = false


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        ctx = parent.context
        val holder = Holder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.payment_methods_list_item,
                parent,
                false
            )
        )

        holder.remove.setOnClickListener {
            onItemClickListener.onRemovePaymentMethodClicked(items!![holder.adapterPosition])
        }
        holder.container.setOnClickListener {
            onItemClickListener.onPaymentMethodClicked(items!![holder.adapterPosition])
        }

        return holder
    }

    var items: List<PaymentMethodEntity>? = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()

        }

    interface ClickListener {
        fun onPaymentMethodClicked(item: PaymentMethodEntity)
        fun onRemovePaymentMethodClicked(item: PaymentMethodEntity)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val item = items?.get(position)
        with(holder) {
            last4.text = item?.last4
            val iconDrawable = when (item?.brand) {
                "visa" -> R.drawable.ic_visacard
                "mastercard" -> R.drawable.ic_mastercard_local
                else -> R.drawable.ic_visacard
            }
            icon.setImageDrawable(ContextCompat.getDrawable(ctx, iconDrawable))
            isDefault.visibility = if (item?.is_default == true) View.VISIBLE else View.INVISIBLE

            remove.visibility = if (showRemoveButton) View.VISIBLE else View.INVISIBLE
        }
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val last4: MyTextView = view.last4
        val icon: ImageView = view.icon
        val isDefault: View = view.isDefault
        val remove: View = view.remove
        val container: View = view.container

    }

}