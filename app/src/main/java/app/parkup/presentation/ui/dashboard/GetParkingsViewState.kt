package app.parkup.presentation.ui.dashboard

import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.UserEntity

data class GetParkingsViewState(
    val showLoading: Boolean = true,
    val data: List<ParkingEntity>? = null
)