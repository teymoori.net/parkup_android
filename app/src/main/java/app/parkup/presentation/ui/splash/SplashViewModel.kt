package app.parkup.presentation.ui.splash

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.use_cases.CheckUserIsLoggedIn
import app.parkup.domain.use_cases.GetParkings
import app.parkup.domain.use_cases.LoginUser
import app.parkup.domain.use_cases.RegisterUser
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.Response
import com.pixabay.utils.tools.log
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val checkUserIsLoggedIn: CheckUserIsLoggedIn
) : BaseViewModel() {

    val userLoggedIn: MutableLiveData<Boolean> = MutableLiveData()

    override fun onViewResumed() {
        super.onViewResumed()
        checkUser()
    }

    private fun checkUser() {

        compositeDisposable.add(
            checkUserIsLoggedIn.observable().subscribe({
                userLoggedIn.value = it
            }, {
                userLoggedIn.value = false
            })
        )

    }

}