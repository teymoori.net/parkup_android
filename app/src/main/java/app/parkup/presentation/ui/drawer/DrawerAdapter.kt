package app.parkup.presentation.ui.drawer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import app.parkup.R

class DrawerAdapter(private var items: List<DrawerItemEntity>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: Holder
        if (convertView == null) {
            view = LayoutInflater.from(parent?.context).inflate(R.layout.drawer_item, parent, false)
            vh = Holder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as Holder
        }
        val item = items[position]

        vh.title.text = item.title.toUpperCase()
        vh.image.setImageDrawable(parent?.context?.let { ContextCompat.getDrawable(it, item.icon) })

        return view
    }

    private class Holder(row: View?) {
        var image: ImageView = row?.findViewById(R.id.image) as ImageView
        var title: TextView = row?.findViewById(R.id.title) as TextView
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }
}