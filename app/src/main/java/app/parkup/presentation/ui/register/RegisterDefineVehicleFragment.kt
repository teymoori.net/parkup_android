package app.parkup.presentation.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.domain.entities.VehicleEntity
import app.parkup.utils.tools.Cons
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.toast
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_register_define_vehicle.*
import kotlinx.android.synthetic.main.fragment_register_get_mail_phone.*
import javax.inject.Inject


class RegisterDefineVehicleFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register_define_vehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(RegisterViewModel::class.java)


        skip.setOnClickListener {
            Navigation.findNavController(save).navigate(
                R.id.action_addVehicleFragment_to_paymentMethodsFragment
            )
        }

        save.setOnClickListener {
            viewModel.addVehicle(
                VehicleEntity(
                    color = color.text.toString(),
                    model = model.text.toString(),
                    plate_number = plate.text.toString()
                )
            )
        }
        viewModel.addVehicleState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> vehicleAdded()
                is Loading -> onLoading(it.isLoading)
                is ErrorIn -> onError(it.message.toString())
            }
        })
    }

    private fun vehicleAdded() {
        loading(false)
        "Your vehicle added successfully".toast(activity)
        Navigation.findNavController(save).navigate(
            R.id.action_addVehicleFragment_to_paymentMethodsFragment
        )
    }


    private fun onLoading(show: Boolean) {
        loading(show)
    }

    private fun onError(msg: String) {
        msg.toast(activity)
    }
}
