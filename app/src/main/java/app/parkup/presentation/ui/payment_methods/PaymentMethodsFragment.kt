package app.parkup.presentation.ui.payment_methods

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.presentation.ui.scan_card.CardDetailsEntity
import app.parkup.utils.tools.Cons
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.views.BaseDialog
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_payment_methods.*
import kotlinx.android.synthetic.main.toolbar.pageTitle
import javax.inject.Inject


class PaymentMethodsFragment : BaseFragment(), PaymentMethodsAdapter.ClickListener {

//    private lateinit var stripe: Stripe

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var paymentMethodsAdapter: PaymentMethodsAdapter

    private lateinit var viewModel: PaymentMethodsViewModel

    var canSkip = false
    lateinit var cardDetails: CardDetailsEntity


    private var removeDialog: BaseDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            canSkip = it.getBoolean(Cons.CAN_SKIP, false)
//            if (it.containsKey(Cons.ITEM_BUNDLE))
//                cardDetails = it.getParcelable(Cons.ITEM_BUNDLE)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_payment_methods, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(PaymentMethodsViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }
        pageTitle.text = "Payment Methods"

        paymentMethodsList.adapter = paymentMethodsAdapter.also {
            it.onItemClickListener = this
            it.showRemoveButton = true
        }

        add.setOnClickListener {
            Navigation.findNavController(skip).navigate(
                R.id.action_paymentMethodsFragment_to_addPaymentMethodFragment
            )
        }

        viewModel.paymentMethodsState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> paymentMethodsAdapter.items = it.data
                is Loading -> {
                }
                is ErrorIn -> showError(true, it.message)
            }
        })

//        viewModel.getStripeDetails.observe(viewLifecycleOwner, Observer {
//            when (it) {
//                is Success -> addPaymentMethod(it.data)
//                is Loading -> {
//                }
//                is ErrorIn -> showError(true, it.message)
//            }
//        })
        viewModel.removeMethod.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                }
                is Loading -> {
                }
                is ErrorIn -> showError(true, it.message)
            }
        })

        skip.visibility = if (canSkip) View.VISIBLE else View.GONE
        skip.setOnClickListener {
            goToDashboard()
        }


//        scanCard.setOnClickListener {
//            checkCameraPermission()
//        }


    }

    private fun goToDashboard() {
        Navigation.findNavController(skip).navigate(
            R.id.action_paymentMethodsFragment_to_dashboardFragment
        )
    }

//    private fun addPaymentMethod(stripeData: StripeSecretEntity?) {
//        stripe = Stripe(activity as Context, stripeData?.publishable_key ?: "")
//        val paymentMethodCard = cardInputWidget.paymentMethodCard
//
//        val billingDetails = PaymentMethod.BillingDetails.Builder()
//            .setEmail(stripeData?.email)
//            .setName(stripeData?.name)
//            .setPhone(stripeData?.phone_number)
//            .build()
//        if (paymentMethodCard != null) {
//            val paymentMethodParams = PaymentMethodCreateParams
//                .create(paymentMethodCard, billingDetails, null)
//            val confirmParams = ConfirmSetupIntentParams
//                .create(paymentMethodParams, stripeData?.client_secret ?: "")
//            stripe.confirmSetupIntent(this, confirmParams)
//        } else {
//            showError(true, "Error in Adding PaymentMethod")
//        }
//    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        stripe.onSetupResult(requestCode, data, object : ApiResultCallback<SetupIntentResult> {
//            override fun onSuccess(result: SetupIntentResult) {
//                val setupIntent = result.intent
//                val status = setupIntent.status
//                if (status == StripeIntent.Status.Succeeded) {
//                    if (canSkip) {
//                        goToDashboard()
//                    }
//                    showError(false, "Payment Method added")
//                    "Payment Method added".log("okhttp stripe_")
//
//                } else if (status == StripeIntent.Status.RequiresPaymentMethod) {
//                    // Setup failed
//                    "failed".log("okhttp stripe_")
//                    showError(true, "Error in Adding PaymentMethod")
//                }
//            }
//
//            override fun onError(e: Exception) {
//                showError(true, e.message)
//                "$e onError".log("stripe_")
//            }
//        })
//    }


    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()

//        if (::cardDetails.isInitialized) {
//            cardInputWidget.setCardNumber(cardDetails.cardNumber)
//            cardInputWidget.setExpiryDate(
//                cardDetails.month?.toInt() ?: 0,
//                cardDetails.year?.toInt() ?: 0
//            )
//        }

    }

    override fun onPaymentMethodClicked(item: PaymentMethodEntity) {
        viewModel.setDefaultPaymentMethod(item)
    }

    override fun onRemovePaymentMethodClicked(item: PaymentMethodEntity) {
        removeDialog = BaseDialog(
            activity as Activity,
            "Are You sure you want to Remove the Payment Method?", "No", "Yes"
        ).setRightClickEvent {
            viewModel.removePaymentMethod(item)
            removeDialog?.dismiss()
            removeDialog?.dialog?.dismiss()
        }.also {
            it.show()
        }
//        viewModel.removePaymentMethod(item)


    }




}
