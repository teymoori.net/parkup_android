package app.parkup.presentation.ui.add_vehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import app.parkup.R
import app.parkup.domain.entities.VehicleEntity
import app.parkup.utils.tools.ColorEntity
import app.parkup.utils.tools.Cons
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.toast
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_add_vehicle.*
import javax.inject.Inject

class AddVehicleFragment : BaseFragment(), ColorsAdapter.ClickListener {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AddVehicleViewModel

    @Inject
    lateinit var colorsAdapter: ColorsAdapter

    var canSkip = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            canSkip = it.getBoolean(Cons.CAN_SKIP, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_vehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(AddVehicleViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }
        pageTitle.text = getString(R.string.add_vehicle)

        addVehicle.setOnClickListener {
            val color = colorsAdapter.items?.find { it.selected == true }

            viewModel.addVehicle(

                VehicleEntity(
                    color = color?.color,
                    color_rgb = color?.rgb,
                    model = vehicleModel.selectedItem?.title,
                    plate_number = plate.text.toString()
                )
            )
        }

        val colors = listOf(
            ColorEntity(
                color = "Grey",
                rgb = "#E0E0E0"
            ),
            ColorEntity(
                color = "Grey",
                rgb = "#757575"
            ),
            ColorEntity(
                color = "Black",
                rgb = "#424242"
            ),
            ColorEntity(
                color = "White",
                rgb = "#FAFAFA"
            ),
            ColorEntity(
                color = "Blue",
                rgb = "#039BE5"
            ),
            ColorEntity(
                color = "Red",
                rgb = "#E53935"
            ),
            ColorEntity(
                color = "Yellow",
                rgb = "#FFEB3B"
            ),
            ColorEntity(
                color = "Green",
                rgb = "#43A047"
            )
        )
        colorsList.layoutManager = GridLayoutManager(activity, 6)
        colorsList.adapter = colorsAdapter.also { it.onItemClickListener = this }
        colorsAdapter.items = colors

        viewModel.addVehicleState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> vehicleAdded()
                is Loading -> onLoading(it.isLoading)
                is ErrorIn -> onError(it.message.toString())
            }
        })


        skip.visibility = if (canSkip) View.VISIBLE else View.GONE
        skip.setOnClickListener {
            goToAddPaymentMethod()
        }

        viewModel.getVehicleModels(activity)

        viewModel.localVehicles.observe(viewLifecycleOwner, Observer {
            vehicleModel.items = it
        })

    }

    private fun vehicleAdded() {
        loading(false)
        "Your vehicle added successfully".toast(activity)
        //vehicleModel.setText("")
        plate.setText("")
        if (canSkip) {
            goToAddPaymentMethod()
        } else {
            activity?.onBackPressed()
        }
    }

    private fun onLoading(show: Boolean) {
        loading(show)
    }

    private fun onError(msg: String) {
        msg.toast(activity)
    }

    override fun onColorClicked(item: ColorEntity?) {
        colorsAdapter.items?.onEach { it.selected = false }
        colorsAdapter.items?.find { it.rgb == item?.rgb }?.selected = true
        colorsAdapter.notifyDataSetChanged()
    }

    private fun goToAddPaymentMethod() {
        Navigation.findNavController(skip).navigate(
            R.id.action_addVehicleFragment_to_paymentMethodsFragment
            ,
            bundleOf(Cons.CAN_SKIP to true)
        )
    }


}
