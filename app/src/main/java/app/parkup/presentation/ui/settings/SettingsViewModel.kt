package app.parkup.presentation.ui.settings

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.*
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val userDetails: GetUserDetails

) : BaseViewModel() {

    val userState: MutableLiveData<Response<UserEntity>> = MutableLiveData()


    override fun onViewResumed() {
        super.onViewResumed()
        getUserDetails()
    }

    private fun getUserDetails() {
        userState.value = Loading(true)

        compositeDisposable.add(
            userDetails.observable().subscribe({
                userState.value = Success(it)
            },
                {
                    userState.value = ErrorIn(it.message)
                })
        )
    }



}