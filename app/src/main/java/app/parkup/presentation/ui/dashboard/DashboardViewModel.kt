package app.parkup.presentation.ui.dashboard

import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.ParkingRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.use_cases.GetParkings
import app.parkup.domain.use_cases.LoginUser
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.tools.log
import javax.inject.Inject

class DashboardViewModel @Inject constructor(
    private val getParkings: GetParkings

) : BaseViewModel() {

    var parkingsViewState: MutableLiveData<List<ParkingEntity>> = MutableLiveData()
    val error: MutableLiveData<String> = MutableLiveData()


    fun getParkings(request:ParkingRequestData) {

        compositeDisposable.add(
            getParkings.getParkings(request).subscribe({
                parkingsViewState.value = it
            },
                {
                    it.toString().log("parksss")
                })
        )

    }

}