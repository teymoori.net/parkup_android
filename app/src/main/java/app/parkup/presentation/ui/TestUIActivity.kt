package app.parkup.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.parkup.R
import kotlinx.android.synthetic.main.activity_test_u_i.*

class TestUIActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_u_i)

        order.setOnClickListener { order.showLoading(true) }
    }
}