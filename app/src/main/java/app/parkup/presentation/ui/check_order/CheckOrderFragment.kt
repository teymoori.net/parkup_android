package app.parkup.presentation.ui.check_order

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import app.parkup.R
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.PriceParkingEntity
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.ImageLoader
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.tools.TimeUtils
import com.pixabay.utils.tools.log
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_check_order.*
import kotlinx.android.synthetic.main.fragment_check_order.pageTitle
import java.util.*
import javax.inject.Inject

class CheckOrderFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var imageLoader: ImageLoader

    private lateinit var viewModel: CheckOrderViewModel
    private var parkingID: Int = 6
    private var priceData: PriceParkingEntity? = null

    lateinit var parking: ParkingEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            parkingID = (it.getString("parkingid") ?: "6").toInt()
            priceData = it.getParcelable(Cons.ITEM_PRICE)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_check_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CheckOrderViewModel::class.java)
        back.setOnClickListener { activity?.onBackPressed() }

        viewModel.parkingViewResult.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                parking = it
                pageTitle.text = it.title
                address.text = it.full_address
                phone.text = it.phone
                imageLoader.load(
                    url = it.image,
                    imageView = parkingImage
                )
                if (priceData == null) {
                    priceData = parking.prices?.get(0)
                }

                showDuration()
            }
        })

        viewModel.vehicleViewResult.observe(viewLifecycleOwner, Observer {
            it?.let {
                carModel.text = it.model ?: ""
                plateNumber.text = "#${it.plate_number}"
                bg.setBackgroundColor(Color.parseColor(it.color_rgb))
            }
        })

        viewModel.paymentMethodViewResult.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                paymentMethodNumber.text = "***** ${it.last4}"
                val iconDrawable = when (it.brand) {
                    "visa" -> R.drawable.ic_visacard
                    "mastercard" -> R.drawable.ic_mastercard
                    else -> R.drawable.ic_visacard
                }
                cardIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        activity as Context,
                        iconDrawable
                    )
                )
                cardIcon.visibility = View.VISIBLE
            } else {
                paymentMethodNumber.text = ""
                cardIcon.visibility = View.INVISIBLE
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                AppUtils.showAlert(activity, null, it, true)
                viewModel.error.postValue(null)
            }
        })

        viewModel.payState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    pay.showLoading(false)
                    loading(false)
                    onPaymentSuccess(it.data)
                }
                is Loading -> {
                    pay.showLoading(true)
                    loading(true)
                }
                is ErrorIn -> {
                    pay.showLoading(false)
                    loading(false)
                    AppUtils.showAlert(activity, null, it.message, true)
                }
            }
        })

        changeVehicle.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_checkOrderFragment_to_vehiclesFragment
            )
        }

        changePaymentMethod.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_checkOrderFragment_to_paymentMethodsFragment,
                bundleOf(Cons.ITEM_BUNDLE to parking)
            )
        }
        changeDuration.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_checkOrderFragment_to_parkingPriceListDialogFragment,
                bundleOf(Cons.ITEM_BUNDLE to parking.prices)
            )
        }

        pay.setOnClickListener {
            viewModel.pay(cost = priceData?.cost, hours = priceData?.hours_expiry)
        }

    }

    private fun showDuration() {
        if (priceData != null) {
            duration.text = priceData?.title
            total.text = "$${priceData?.cost}"
            val hoursExpire = priceData?.hours_expiry ?: 0

            val cal: Calendar = Calendar.getInstance()
            cal.time = Date()
            cal.add(Calendar.HOUR_OF_DAY, hoursExpire)
            val expireTime = cal.time
            willExpire.text =
                "Parking ends ".plus(TimeUtils.dateToFormat(expireTime, "dd MMM yyyy h:m a"))
        }
    }

    private fun onPaymentSuccess(orderId: Int?) {
        AppUtils.showAlert(activity, null, "Payment successful", false)
        Navigation.findNavController(pageTitle).navigate(
            Uri.parse(AppUtils.getOrderDeepLinkUri(orderId))
        )
    }


    override fun onResume() {
        super.onResume()
        viewModel.getData(parkingID)
        showDuration()
    }
}