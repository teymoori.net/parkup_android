package app.parkup.presentation.ui.register

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.AddVehicle
import app.parkup.domain.use_cases.RegisterUser
import com.onesignal.OneSignal
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Response
import com.pixabay.utils.models.Success
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val registerUser: RegisterUser,
    private val addVehicle: AddVehicle
) : BaseViewModel() {

    var registerState: MutableLiveData<Response<UserEntity>> = MutableLiveData()
    var addVehicleState: MutableLiveData<Response<VehicleEntity>> = MutableLiveData()

    fun register(name_family: String, email: String, password: String) {

        registerState.value = Loading(true)

        compositeDisposable.add(
            registerUser.register(
                name_family = name_family,
                email = email,
                password = password
            ).subscribe({
                OneSignal.sendTag("USER_EMAIL", it.email)
                registerState.value = Success(data = it)
            },
                {
                    registerState.value = ErrorIn(message = it.message)
                })
        )
    }

    fun addVehicle(vehicleEntity: VehicleEntity){
        addVehicleState.value = Loading(true)

        compositeDisposable.add(
            addVehicle.addVehicle(
                vehicleEntity
            ).subscribe({
                addVehicleState.value = Success(data = it)
            },
                {
                    addVehicleState.value = ErrorIn(message = it.message)
                })
        )
    }

}