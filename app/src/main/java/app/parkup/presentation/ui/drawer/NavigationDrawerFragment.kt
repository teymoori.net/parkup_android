package app.parkup.presentation.ui.drawer

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import app.parkup.R
import app.parkup.domain.entities.UserEntity
import app.parkup.presentation.ui.dashboard.DashboardFragment
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.views.BaseDialog
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_navigation_drawer.*
import javax.inject.Inject

class NavigationDrawerFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: DrawerViewModel

    private var logOutDialog: BaseDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(DrawerViewModel::class.java)

        viewModel.userDetails.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> showUserDetails(it.data)
            }
        })

        drawerList.adapter = DrawerAdapter(DrawerUtil.getDrawerItems())
        drawerList.setOnItemClickListener { adapterView, _, pos, _ ->
            onDrawerItemClicked(adapterView.getItemAtPosition(pos) as DrawerItemEntity)
        }


        appVersion.text = AppUtils.appVersion(activity)

    }

    private fun onDrawerItemClicked(item: DrawerItemEntity) {
        when (item.type) {
            DrawerItemType.PAYMENT_METHODS -> paymentMethods()
            DrawerItemType.VEHICLES -> vehicles()
            DrawerItemType.HISTORY -> history()
            DrawerItemType.TERMS -> terms();
            DrawerItemType.HELP -> TODO()
            DrawerItemType.LOGOUT -> logOut()
            DrawerItemType.SETTING -> settings()
        }
        (parentFragment as? DashboardFragment)?.toggleMenu()
    }

    private fun paymentMethods() {
        NavHostFragment.findNavController(this).navigate(
            R.id.action_dashboardFragment_to_paymentMethodsFragment
        )
    }

    private fun vehicles() {
        NavHostFragment.findNavController(this).navigate(
            R.id.action_dashboardFragment_to_vehiclesFragment
        )
    }

    private fun history() {
        NavHostFragment.findNavController(this).navigate(
            R.id.action_dashboardFragment_to_ordersListFragment
        )
    }

    private fun settings() {
        NavHostFragment.findNavController(this).navigate(
            R.id.action_dashboardFragment_to_settingsFragment
        )
    }

    private fun terms() {
        NavHostFragment.findNavController(this).navigate(
            R.id.action_dashboardFragment_to_termsAndConditionsFragment
        )
    }

    private fun logOut() {

        logOutDialog = BaseDialog(
            activity as Activity,
            "Are You sure you want to Log Out?", "No", "Yes"
        ).setRightClickEvent {
            viewModel.logOut()

            NavHostFragment.findNavController(this).navigate(
                R.id.action_dashboardFragment_to_loginFragment
            )
            logOutDialog?.dismiss()
            logOutDialog?.dialog?.dismiss()

        }.also {
            it.show()
        }
    }


    private fun showUserDetails(user: UserEntity?) {
        nameFamily.text = user?.name
    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

}
