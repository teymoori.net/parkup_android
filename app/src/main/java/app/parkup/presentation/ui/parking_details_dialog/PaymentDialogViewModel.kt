package app.parkup.presentation.ui.parking_details_dialog

import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.*
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class PaymentDialogViewModel @Inject constructor(
    private val getOrder: GetOrder,
    private val getPaymentMethods: GetPaymentMethods,
    private val defaultPaymentMethod: SetDefaultPaymentMethod,
    private val pay: Pay
) : BaseViewModel() {

    var order: OrderEntity? = null
    var paymentMethod: PaymentMethodEntity? = null

    val payState: MutableLiveData<Response<PaymentEntity>> = MutableLiveData()
    val orderState: MutableLiveData<Response<OrderEntity>> = MutableLiveData()

    val paymentMethodsState: MutableLiveData<Response<List<PaymentMethodEntity>>> =
        MutableLiveData()


    override fun onViewResumed() {
        super.onViewResumed()
        getPaymentMethods()
    }

    fun getOrder(orderID: Int?) {
        if (orderID != null) {
            orderState.value = Loading(true)
            compositeDisposable.add(
                getOrder.getOrder(orderID).subscribe({
                    order = it
                    orderState.value = Success(it)
                },
                    {
                        orderState.value = ErrorIn(it.message)
                    })
            )
        }
    }

    fun getPaymentMethods() {
        paymentMethodsState.value = Loading(true)
        compositeDisposable.add(
            getPaymentMethods.observable().subscribe({
                if (it.isNotEmpty()) {
                    paymentMethod = it.find { it.is_default == true }
                }
                paymentMethodsState.value = Success(it)
            },
                {
                    paymentMethodsState.value = ErrorIn(it.message)
                })
        )
    }

    fun setDefaultPaymentMethod(paymentMethodEntity: PaymentMethodEntity) {
        compositeDisposable.add(
            defaultPaymentMethod.setMethod(paymentMethodEntity).subscribe({
                getPaymentMethods()
            },
                {
                    getPaymentMethods()
                })
        )
    }


    fun pay() {
        if (order == null) {
            payState.value = ErrorIn("order empty")
            return
        }
        if (paymentMethod == null) {
            payState.value = ErrorIn("Default payment method not found")
            return
        }

        compositeDisposable.add(
            pay.pay(order?.id?.toLong() ?: 0).subscribe({
                it.toString().log("payyy")
                if (it.status == "succeeded") {
                    payState.value = Success(it)
                } else {
                    payState.value = ErrorIn(it.cancellation_reason)
                }
            },
                {
                    it.toString().log("payyy")
                })
        )


    }


}