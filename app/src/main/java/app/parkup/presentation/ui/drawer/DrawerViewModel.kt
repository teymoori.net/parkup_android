package app.parkup.presentation.ui.drawer

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.use_cases.GetParkings
import app.parkup.domain.use_cases.GetUserDetails
import app.parkup.domain.use_cases.LogOutUser
import app.parkup.domain.use_cases.LoginUser
import app.parkup.presentation.ui.login.LoginViewModel
import com.onesignal.OneSignal
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Response
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.log
import javax.inject.Inject

class DrawerViewModel @Inject constructor(
    private val getUserDetails: GetUserDetails ,
    private val logOutUser: LogOutUser

) : BaseViewModel() {

    val userDetails: MutableLiveData<Response<UserEntity>> = MutableLiveData()

    override fun onViewResumed() {
        super.onViewResumed()
        getUserDetails()
    }

    private fun getUserDetails() {
        compositeDisposable.add(
            getUserDetails.observable().subscribe({
                userDetails.value = Success<UserEntity>(data = it)
            },
                {
                    userDetails.value = ErrorIn(message = it.message)
                })
        )
    }


    fun logOut(){
        OneSignal.setSubscription(false)

        compositeDisposable.add(
            logOutUser.observable().subscribe({

            },
                {
                    userDetails.value = ErrorIn(message = it.message)
                })
        )
    }

}