package app.parkup.presentation.ui.order_detail

import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.AddOrder
import app.parkup.domain.use_cases.GetOrder
import app.parkup.domain.use_cases.GetVehicles
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class OrderDetailViewModel @Inject constructor(
    private val getOrder: GetOrder

) : BaseViewModel() {

    val orderState: MutableLiveData<Response<OrderEntity>> = MutableLiveData()


    fun getOrder(orderID: Int?) {
        if (orderID != null) {
            orderState.value = Loading(true)
            compositeDisposable.add(
                getOrder.getOrder(orderID).subscribe({
                    it.toString().log("order_")
                    orderState.value = Success(it)
                },
                    {
                        it.message.toString().log("order_")
                        orderState.value = ErrorIn(it.message)
                    })
            )
        }
    }


}