package app.parkup.presentation.ui.orders

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import app.parkup.R
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.tools.log
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_orders_list.*
import javax.inject.Inject


class OrdersListFragment : BaseFragment(), OrdersAdapter.ClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: OrdersViewModel

    @Inject
    lateinit var ordersAdapter: OrdersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_orders_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(OrdersViewModel::class.java)

        pageTitle.text = getString(R.string.orders_history)
        back.setOnClickListener { activity?.onBackPressed() }

        orders.adapter = ordersAdapter
        orders.setOnItemClickListener { adapter, _, pos, _ ->
            onOrderClicked(
                adapter.getItemAtPosition(
                    pos
                ) as? OrderEntity
            )
        }

        viewModel.ordersState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    loading(false)
                    it.data?.size.toString().log("orders__")
                    ordersAdapter.items = it.data
                }
                is Loading -> {
                    loading(it.isLoading)
                }
                is ErrorIn -> {
                    loading(false)
                    it.message.toString().log("orders__")
                }
            }
        })
    }

    override fun onOrderClicked(item: OrderEntity?) {
        NavHostFragment.findNavController(this).navigate(
            Uri.parse(AppUtils.getOrderDeepLinkUri(item?.id))
        )
    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

}