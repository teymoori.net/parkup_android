package app.parkup.presentation.ui.container

import android.os.Bundle
import app.parkup.R
import com.pixabay.utils.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
