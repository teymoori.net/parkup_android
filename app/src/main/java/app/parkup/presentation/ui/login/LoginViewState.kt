package app.parkup.presentation.ui.login

import app.parkup.domain.entities.UserEntity

data class LoginViewState(
    val showLoading: Boolean = true,
    val data: UserEntity? = null
)