package app.parkup.presentation.ui.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.Cons.Companion.CAN_SKIP
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.toast
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_register_get_mail_phone.*
import javax.inject.Inject

class RegisterGetMailPhoneFragment : BaseFragment() {

    lateinit var nameFamily: String

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            nameFamily = it.getString(Cons.ITEM_BUNDLE)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register_get_mail_phone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(RegisterViewModel::class.java)

        register.setOnClickListener {
            if(email.text.isNullOrBlank()){
                email.error = "Email is Mandatory"
                return@setOnClickListener
            }
            if(password.text.isNullOrBlank()){
                password.error = "Password is Mandatory"
                return@setOnClickListener
            }
            if(password.text.toString() != confirmPassword.text.toString()){
                confirmPassword.error = "Password and Confirm Password not matched."
                return@setOnClickListener
            }

            viewModel.register(
                name_family = nameFamily,
                email = email.text.toString(),
                password = password.text.toString()
            )
        }

        viewModel.registerState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> registered()
                is Loading -> loading(it.isLoading)
                is ErrorIn -> onError(it.message)
            }
        })

    }
    private fun registered(){
        loading(false)
        "Your are registered successfully".toast(activity)
        Navigation.findNavController(register).navigate(
            R.id.action_registerGetMailPhoneFragment_to_addVehicleFragment
            ,
            bundleOf(CAN_SKIP to true)
        )
    }
    private fun onError(msg:String?){
        loading(false)
    }

}
