package app.parkup.presentation.ui.add_vehicle

import android.content.Context
import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.LocalVehicleData
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.AddVehicle
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Response
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.tools.log
import my.mobilityone.onecent.ui.spinner.SpinnerDialogFragment
import java.util.*
import javax.inject.Inject


class AddVehicleViewModel @Inject constructor(
    private val addVehicle: AddVehicle

) : BaseViewModel() {

    var addVehicleState: MutableLiveData<Response<VehicleEntity>> = MutableLiveData()
    var localVehicles: MutableLiveData<List<SpinnerDialogFragment.SpinnerDataModel>> = MutableLiveData()


    fun addVehicle(vehicleEntity: VehicleEntity) {
        addVehicleState.value = Loading(true)

        compositeDisposable.add(
            addVehicle.addVehicle(
                vehicleEntity
            ).subscribe({
                addVehicleState.value = Success(data = it)

            },
                {
                    addVehicleState.value = ErrorIn(message = it.message)
                })
        )
    }


    fun getVehicleModels(ctx: Context?) {
        val data = AppUtils.assetsToString(ctx, "cars.json")
        val listType = object : TypeToken<List<LocalVehicleData>>() {
        }.type
        val vehicles = Gson().fromJson<List<LocalVehicleData>>(data, listType) .map {
            SpinnerDialogFragment.SpinnerDataModel(
                title = it.name,
                id = it.id
            )
        }

        localVehicles.postValue(vehicles)

    }


}