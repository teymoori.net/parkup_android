package app.parkup.presentation.ui.parking_price_list_dialog

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import app.parkup.R
import app.parkup.domain.entities.PriceParkingEntity
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.tools.TimeUtils.Companion.dateTimeStringToDate
import com.pixabay.utils.tools.listen
import kotlinx.android.synthetic.main.orders_item.view.*
import javax.inject.Inject

class PriceParkingListAdapter @Inject constructor() :
    BaseAdapter() {
    var items: List<PriceParkingEntity>? = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    interface ClickListener {
        fun onPriceClicked(item: PriceParkingEntity?)
    }

    private class Holder(view: View) {
        var duration: TextView = view.findViewById(R.id.duration)
        var price: TextView = view.findViewById(R.id.price)
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, cv: View?, parent: ViewGroup?): View {
        val viewHolder: Holder
        var convertView = cv
        if (convertView == null) {
            convertView =
                LayoutInflater.from(parent?.context)
                    .inflate(R.layout.price_list_item, parent, false)
            viewHolder = Holder(convertView)
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as Holder
        }

        val item = items?.get(position)
        viewHolder.duration.text = item?.title
        viewHolder.price.text = "$${item?.cost}"

        return convertView!!

    }

    override fun getItem(position: Int): PriceParkingEntity? {
        return items?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.size ?: 0
    }


}