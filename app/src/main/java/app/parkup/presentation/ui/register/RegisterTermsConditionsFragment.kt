package app.parkup.presentation.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import app.parkup.R
import kotlinx.android.synthetic.main.fragment_register_terms_conditions.*

class RegisterTermsConditionsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register_terms_conditions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disagree.setOnClickListener { activity?.onBackPressed() }
        agree.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_registerTermsConditionsFragment_to_registerGetUserPersonalDetailsFragment
            )
        }
    }
}