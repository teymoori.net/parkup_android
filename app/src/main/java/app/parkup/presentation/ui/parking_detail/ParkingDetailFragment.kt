package app.parkup.presentation.ui.parking_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.parkup.R
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.ImageLoader
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.tools.TimeUtils
import com.pixabay.utils.tools.log
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_parking_detail.*
import kotlinx.android.synthetic.main.fragment_parking_detail.durationVU
import kotlinx.android.synthetic.main.fragment_parking_detail.pageTitle
import kotlinx.android.synthetic.main.fragment_parking_detail.rating
import java.util.*
import javax.inject.Inject

class ParkingDetailFragment : BaseFragment(), VehiclesHorizontalAdapter.ClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ParkingDetailsViewModel

    lateinit var parking: ParkingEntity

    @Inject
    lateinit var imageLoader: ImageLoader

    @Inject
    lateinit var vehiclesAdapter: VehiclesHorizontalAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            parking = it.getParcelable(Cons.ITEM_BUNDLE)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_parking_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ParkingDetailsViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }

        with(parking) {
            pageTitle.text = title
            address.text = full_address
            number.text = phone
            costTitle.text = "$${cost}"
            durationVU.text = duration_text
            parkingDescription.text = description
            hoursOperation.text = "${parking.hours_operation}"
            rating.rating = rank?.toFloat() ?: 3f
          //  prices.text = "$${monthly_cost} Monthly"
            imageLoader.load(
                url = image,
                imageView = parkingImage
            )
            if (start != null) {
                val startDate = Date(start.toLong() * 1000)
                startTimeVU.text = TimeUtils.dateToFormat(startDate, "h:ma, MMM dd")
            }
            if (end != null) {
                val endDate = Date(end.toLong() * 1000)
                endTimeVU.text = TimeUtils.dateToFormat(endDate, "h:ma, MMM dd")
            }

            order.setOnClickListener {
                viewModel.addOrder(
                    parking = this,
                    vehicle = vehiclesAdapter.items?.first { it.selected == true },
                    start = start?.toLong() ?: 0 * 1000,
                    end = end?.toLong() ?: 0 * 1000
                )
            }
        }

        vehicles.adapter = vehiclesAdapter.also { it.onItemClickListener = this }

        viewModel.vehiclesState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> vehiclesAdapter.items = it.data
                is Loading -> {
                }
                is ErrorIn -> {
                }
            }
        })
        viewModel.addOrderState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> {
                    NavHostFragment.findNavController(this).navigate(
                        R.id.action_parkingDetailFragment_to_orderDetailsFragment
                        ,
                        bundleOf(Cons.ITEM_BUNDLE to it.data?.id)
                    )
                }
                is Loading -> {

                }
                is ErrorIn -> {
                    it.message.toString().log("order_")
                }
            }
        })


    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
    }

    override fun onVehicleClicked(item: VehicleEntity?) {

        vehiclesAdapter.items?.onEach { it.selected = false }
        vehiclesAdapter.items?.find { it.id == item?.id }?.selected = true
        vehiclesAdapter.notifyDataSetChanged()

        if (item?.id == -1) {
            NavHostFragment.findNavController(this).navigate(
                R.id.action_parkingDetailFragment_to_addVehicleFragment
            )
        }
    }


}
