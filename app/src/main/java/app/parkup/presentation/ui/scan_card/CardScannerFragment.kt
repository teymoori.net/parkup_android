package app.parkup.presentation.ui.scan_card

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.utils.tools.Cons
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.pixabay.utils.tools.ScreenUtils.Companion.dpToPx
import com.pixabay.utils.tools.ScreenUtils.Companion.getScreenWidth
import com.pixabay.utils.tools.log
import com.pixabay.utils.tools.toast
import kotlinx.android.synthetic.main.fragment_card_scanner.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import java.io.ByteArrayOutputStream
import java.io.File

class CardScannerFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_card_scanner, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        camera.setLifecycleOwner(this)
        camera.layoutParams.height =
            (((getScreenWidth(activity as Activity) - dpToPx(64)) / 1.5)).toInt()



        captureBtn.setOnClickListener {
            captureBtn.playAnimation()
            takePhoto()
        }

        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                result.toFile(
                    File(getFilePath())
                ) { file: File? ->
                    val bm = BitmapFactory.decodeFile(getFilePath())
                    val outputStream = ByteArrayOutputStream()
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                    extractPlate(bm)
                }
            }
        })

    }

    var fileName: String? = null
    private fun takePhoto() {
        fileName = System.currentTimeMillis().toString()
        camera.takePictureSnapshot();
    }

    fun getFilePath(): String {
        val directory: File =
            ContextWrapper(activity).getDir("imageDir", Context.MODE_PRIVATE)
        return File(directory, "$fileName.jpg").absolutePath
    }

    private fun extractPlate(bitmap: Bitmap) {
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val detector =
            FirebaseVision.getInstance().onDeviceTextRecognizer
        detector.processImage(image)
            .addOnSuccessListener { texts -> getDetails(texts) }
            .addOnFailureListener { e -> e.printStackTrace() }
    }


    private fun getDetails(texts: FirebaseVisionText?) {


        if (texts != null) {
            val details = CardDetailsEntity()

            val blocks = texts.textBlocks

            for (block in blocks) {
                for (line in block.lines) {
                    val txt = line.text


                    txt.log("mlkitt")


                    if (txt.length in 11..30 && ".*\\d.*".toRegex()
                            .containsMatchIn(txt)
                        && !"[a-zA-Z]+".toRegex()
                            .containsMatchIn(txt)
                    ) {
                        details.cardNumber = txt
                    }

                    if (txt.contains("/")) {

                        val yearMonth = txt.replace("O", "0").filter { it.isDigit() }
                        yearMonth.log("mlkitt yearMonth")
                        details.month = yearMonth.take(2)
                        details.year = yearMonth.takeLast(2)

                    }

                    line.text.log("mlkitt___")

                    if (details.cardNumber != null && details.month != null && details.year != null)
                        break

                }
                if (details.cardNumber != null && details.month != null && details.year != null)
                    break

            }

            details.toString().log("mlkitt___")

//            Navigation.findNavController(captureBtn).navigate(
//                R.id.action_cardScannerFragment_to_addPaymentMethodFragment,
//                bundleOf(Cons.ITEM_BUNDLE to details)
//            )

        }

    }
}