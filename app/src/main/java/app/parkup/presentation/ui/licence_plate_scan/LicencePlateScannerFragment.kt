package app.parkup.presentation.ui.licence_plate_scan

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.parkup.R
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.pixabay.utils.tools.ScreenUtils
import com.pixabay.utils.tools.log
import kotlinx.android.synthetic.main.fragment_licence_plate_scanner.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class LicencePlateScannerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_licence_plate_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        camera.setLifecycleOwner(this)
        showIdCard()

        captureBtn.setOnClickListener {
            captureBtn.playAnimation()
            takePhoto()
        }

        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                result.toFile(
                    File(getFilePath())
                ) { file: File? ->
                    convertAndSavePhoto(file)
                }
            }
        })


        ///timer
        Timer().scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                takePhoto()
            }
        }, 0, 4000)


    }


    private fun convertAndSavePhoto(imageFile: File?) {
        if (imageFile != null) {
            val bm = BitmapFactory.decodeFile(getFilePath())
            val outputStream = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            extractPlate(bm)
        }
    }

    var fileName: String? = null
    private fun takePhoto() {
        fileName = System.currentTimeMillis().toString()
        camera.takePictureSnapshot();
    }

    fun getFilePath(): String {
        val directory: File =
            ContextWrapper(activity).getDir("imageDir", Context.MODE_PRIVATE)
        return File(directory, "$fileName.jpg").absolutePath
    }


    private fun showIdCard() {
        frame.setImageResource(R.drawable.ic_id_card_frame)
        camera.layoutParams.height =
            (((ScreenUtils.getScreenWidth(activity) - ScreenUtils.dpToPx(64)) / 1.5)).toInt()
    }


    private fun extractPlate(bitmap: Bitmap) {
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val detector =
            FirebaseVision.getInstance().onDeviceTextRecognizer
        detector.processImage(image)
            .addOnSuccessListener { texts -> showText(texts) }
            .addOnFailureListener { e -> e.printStackTrace() }
    }

    private fun showText(texts: FirebaseVisionText) {
        val blocks = texts.textBlocks

        for (block in blocks) {
            val txt = block.text
            txt.log("mlkitt")
            if (txt.length in 4..13 && ".*\\d.*".toRegex()
                    .containsMatchIn(txt)
                && "[a-zA-Z]+".toRegex()
                    .containsMatchIn(txt)
            ) {
                licencePlate.text = txt
                break
            }
        }


    }
}