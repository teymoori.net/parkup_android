package app.parkup.presentation.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.utils.tools.Cons
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_register_get_user_personal_details.*
import javax.inject.Inject

class RegisterGetUserPersonalDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_register_get_user_personal_details,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        next.setOnClickListener {
            if (name_family.text.toString().isBlank())
                name_family.error = getString(R.string.name_family_mandatory)
            else
                Navigation.findNavController(it).navigate(
                    R.id.action_registerGetUserPersonalDetailsFragment_to_registerGetMailPhoneFragment
                    ,
                    bundleOf(Cons.ITEM_BUNDLE to name_family.text.toString())
                )
        }
    }

}
