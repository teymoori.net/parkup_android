package app.parkup.presentation.ui.parking_detail

import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.AddOrder
import app.parkup.domain.use_cases.GetVehicles
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class ParkingDetailsViewModel @Inject constructor(
    private val getVehicles: GetVehicles,
    private val addOrder: AddOrder

) : BaseViewModel() {

    val vehiclesState: MutableLiveData<Response<List<VehicleEntity>>> = MutableLiveData()
    val addOrderState: MutableLiveData<Response<OrderEntity>> = MutableLiveData()


    override fun onViewResumed() {
        super.onViewResumed()
        getVehicles()
    }

    private fun getVehicles() {
        val addNewVehicle = VehicleEntity(
            id = -1
        )
        vehiclesState.value = Loading(true)
        compositeDisposable.add(
            getVehicles.observable().subscribe({
                val data: MutableList<VehicleEntity> = mutableListOf()
                data.addAll(it)
                data.add(addNewVehicle)
                data[0].selected = true
                vehiclesState.value = Success(data as List<VehicleEntity>)
            },
                {
                    vehiclesState.value = ErrorIn(it.message)
                })
        )
    }


    fun addOrder(parking: ParkingEntity, vehicle: VehicleEntity?, start: Long, end: Long) {
        if (vehicle == null) {
            addOrderState.value = ErrorIn("Vehicle is empty")
            return
        }
        if (start.compareTo(0) == 0 || end.compareTo(0) == 0 || end <= start) {
            addOrderState.value = ErrorIn("Date Time not correct")
            return
        }

        addOrderState.value = Loading(true)

//        compositeDisposable.add(
//            addOrder.addOrder(
//                AddOrderRequestData(
//                    vehicleID = vehicle.id,
//                    parkingID = parking.id,
//                    start = start,
//                    end = end
//                )
//            ).subscribe({
//                addOrderState.value = Success(it)
//            }, {
//                it.message.toString().log("order_")
//            }
//            )
//        )

    }

}