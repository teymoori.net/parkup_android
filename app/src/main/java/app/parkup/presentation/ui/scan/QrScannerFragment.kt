package app.parkup.presentation.ui.scan

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import app.parkup.R
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pixabay.utils.tools.toast
import kotlinx.android.synthetic.main.fragment_qr_scanner.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class QrScannerFragment : Fragment() {
    val CAMERA_CODE = 1001

    private lateinit var photoFile: File
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_qr_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.plate)

//        val image = FirebaseVisionImage.fromBitmap(bitmap)
//        val textRecognizer = FirebaseVision.getInstance().onDeviceTextRecognizer
//        textRecognizer.processImage(image)
//            .addOnSuccessListener {
//                it.text.toast(activity)
//            }
//            .addOnFailureListener {
//
//            }

        scan.setOnClickListener {
            checkCameraPermission()
        }
    }


    private fun checkCameraPermission() {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    openCameraIntent()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {/* ... */
                }
            }).check()
    }

    fun openCameraIntent() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (activity?.packageManager?.let { pictureIntent.resolveActivity(it) } != null) {

            try {
                photoFile = createImageFile();
            } catch (e: Exception) {

            }
            val photoURI: Uri = FileProvider.getUriForFile(
                activity as Activity,
                "app.parkup.fileprovider",
                photoFile
            );
            pictureIntent.putExtra(
                MediaStore.EXTRA_OUTPUT,
                photoURI
            );
            startActivityForResult(
                pictureIntent,
                CAMERA_CODE
            );
        }
    }

    private var imageFilePath: String? = null
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.getDefault()
        ).format(Date())
        val imageFileName = "IMG_" + timeStamp + "_"
        val storageDir = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        imageFilePath = image.absolutePath
        return image
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_CODE) {

                val filePath: String = photoFile.getPath()
                val bitmap = BitmapFactory.decodeFile(filePath)
                extractTexts(bitmap)
            }
        }
    }


    private fun extractTexts(bitmap:Bitmap){
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val textRecognizer = FirebaseVision.getInstance().onDeviceTextRecognizer
        textRecognizer.processImage(image)
            .addOnSuccessListener {
                it.text.toast(activity)
                results.text = it.text
            }
            .addOnFailureListener {

            }
    }

}