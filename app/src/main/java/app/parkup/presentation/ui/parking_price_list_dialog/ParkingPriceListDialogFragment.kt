package app.parkup.presentation.ui.parking_price_list_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import app.parkup.R
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.PriceParkingEntity
import app.parkup.utils.tools.Cons
import com.pixabay.utils.tools.toast
import com.pixabay.utils.views.ExpandedBottomSheetDialog
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_parking_price_list_dialog.*
import javax.inject.Inject

class ParkingPriceListDialogFragment : ExpandedBottomSheetDialog() {

    lateinit var prices: List<PriceParkingEntity>

    @Inject
    lateinit var adapter: PriceParkingListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            prices = it.getParcelableArrayList(Cons.ITEM_BUNDLE)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_parking_price_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        priceListView.adapter = adapter.also { it.items = prices }

        priceListView.setOnItemClickListener { _, _, position, id ->

            NavHostFragment.findNavController(this).navigate(
                R.id.action_parkingPriceListDialogFragment_to_checkOrderFragment,
                bundleOf(Cons.ITEM_PRICE to prices[position])
            )
        }
    }
}