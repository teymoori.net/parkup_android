package app.parkup.presentation.ui.payment_methods

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.entities.StripeSecretEntity
import app.parkup.domain.use_cases.GetPaymentMethods
import app.parkup.domain.use_cases.GetStripeSecrets
import app.parkup.domain.use_cases.RemovePaymentMethod
import app.parkup.domain.use_cases.SetDefaultPaymentMethod
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import javax.inject.Inject

class PaymentMethodsViewModel @Inject constructor(
    private val getPaymentMethods: GetPaymentMethods,
    private val defaultPaymentMethod: SetDefaultPaymentMethod,
    private val removePaymentMethod: RemovePaymentMethod,
    private val getStripeSecrets: GetStripeSecrets
) : BaseViewModel() {

    val paymentMethodsState: MutableLiveData<Response<List<PaymentMethodEntity>>> =
        MutableLiveData()


    val getStripeDetails: MutableLiveData<Response<StripeSecretEntity>> =
        MutableLiveData()

    val setDefaultMethod: MutableLiveData<Response<Boolean>> =
        MutableLiveData()

    val removeMethod: MutableLiveData<Response<Boolean>> =
        MutableLiveData()


    override fun onViewResumed() {
        super.onViewResumed()
        getPaymentMethods()
    }


    fun removePaymentMethod(paymentMethodEntity: PaymentMethodEntity) {
        removeMethod.value = Loading(true)
        compositeDisposable.add(
            removePaymentMethod.removeMethod(paymentMethodEntity).subscribe({
                removeMethod.value = Success(true)
                getPaymentMethods()
            },
                {
                    removeMethod.value = ErrorIn(it?.message)
                    getPaymentMethods()
                })
        )
    }
    fun setDefaultPaymentMethod(paymentMethodEntity: PaymentMethodEntity) {
        setDefaultMethod.value = Loading(true)
        compositeDisposable.add(
            defaultPaymentMethod.setMethod(paymentMethodEntity).subscribe({
                setDefaultMethod.value = Success(true)
                getPaymentMethods()
            },
                {
                    setDefaultMethod.value = ErrorIn(it?.message)
                    getPaymentMethods()
                })
        )
    }


    private fun getPaymentMethods() {
        paymentMethodsState.value = Loading(true)
        compositeDisposable.add(
            getPaymentMethods.observable().subscribe({
                paymentMethodsState.value = Success(it)
            },
                {
                    paymentMethodsState.value = ErrorIn(it.message)
                })
        )
    }


    fun getStripeDetails() {
        getStripeDetails.value = Loading(true)
        compositeDisposable.add(
            getStripeSecrets.observable().subscribe({
                getStripeDetails.value = Success(it)
            },
                {
                    getStripeDetails.value = ErrorIn(it.message)
                })
        )
    }

}