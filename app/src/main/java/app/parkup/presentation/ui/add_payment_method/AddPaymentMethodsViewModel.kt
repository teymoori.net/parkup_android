package app.parkup.presentation.ui.add_payment_method

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.entities.StripeSecretEntity
import app.parkup.domain.use_cases.GetPaymentMethods
import app.parkup.domain.use_cases.GetStripeSecrets
import app.parkup.domain.use_cases.RemovePaymentMethod
import app.parkup.domain.use_cases.SetDefaultPaymentMethod
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import javax.inject.Inject

class AddPaymentMethodsViewModel @Inject constructor(
    private val getStripeSecrets: GetStripeSecrets
) : BaseViewModel() {

    val getStripeDetails: MutableLiveData<Response<StripeSecretEntity>> =
        MutableLiveData()

    fun getStripeDetails() {
        getStripeDetails.value = Loading(true)
        compositeDisposable.add(
            getStripeSecrets.observable().subscribe({
                getStripeDetails.value = Success(it)
            },
                {
                    getStripeDetails.value = ErrorIn(it.message)
                })
        )
    }


}