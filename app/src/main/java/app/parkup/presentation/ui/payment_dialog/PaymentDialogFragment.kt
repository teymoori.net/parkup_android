package app.parkup.presentation.ui.payment_dialog


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import app.parkup.R
import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.presentation.ui.parking_details_dialog.PaymentDialogViewModel
import app.parkup.presentation.ui.payment_methods.PaymentMethodsAdapter
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.views.ExpandedBottomSheetDialog
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_payment_dialog.*
import kotlinx.android.synthetic.main.fragment_payment_dialog.paymentMethodsList
import javax.inject.Inject

class PaymentDialogFragment : ExpandedBottomSheetDialog(), PaymentMethodsAdapter.ClickListener {

    var orderID: Int = 0

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: PaymentDialogViewModel


    @Inject
    lateinit var paymentMethodsAdapter: PaymentMethodsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            orderID = it.getInt(Cons.ITEM_BUNDLE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_payment_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(PaymentDialogViewModel::class.java)

        pay.setOnClickListener {
            viewModel.pay()
        }
        addPaymentMethod.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(
                R.id.action_paymentDialogFragment_to_paymentMethodsFragment
            )
        }

        viewModel.orderState.observe(this, Observer {
            when (it) {
                is Success -> showOrderDetails(it.data)
                is Loading -> {

                }
                is ErrorIn -> {

                }
            }
        })
        viewModel.paymentMethodsState.observe(this, Observer {
            when (it) {
                is Success -> showPaymentMethods(it.data)
                is Loading -> {

                }
                is ErrorIn -> {

                }
            }
        })
        viewModel.payState.observe(this, Observer {
            when (it) {
                is Success -> onPaymentSuccess(it.data)
                is Loading -> {

                }
                is ErrorIn -> {
                    AppUtils.showAlert(activity, null, it.message, true)
                }
            }
        })

        paymentMethodsList.adapter = paymentMethodsAdapter.also { it.onItemClickListener = this }

    }

    private fun onPaymentSuccess(payment: PaymentEntity?) {
        AppUtils.showAlert(activity, null, "Payment successful", false)
        activity?.startActivityForResult(activity?.intent, 10);
        this.dismiss()

    }

    private fun showPaymentMethods(paymentMethods: List<PaymentMethodEntity>?) {
        if (paymentMethods?.isNotEmpty() == true) {
            addPaymentMethod.visibility = View.GONE
            pay.visibility = View.VISIBLE
        } else {
            addPaymentMethod.visibility = View.VISIBLE
            pay.visibility = View.GONE
        }
        paymentMethodsAdapter.items = paymentMethods
    }

    @SuppressLint("SetTextI18n")
    private fun showOrderDetails(order: OrderEntity?) {
        amount.text = "$${order?.cost}"
        orderIDView.text = "${order?.order_code}"
    }

    override fun onResume() {
        super.onResume()
        viewModel.onViewResumed()
        viewModel.getOrder(orderID)
    }

    override fun onPaymentMethodClicked(item: PaymentMethodEntity) {
        viewModel.setDefaultPaymentMethod(item)
    }

    override fun onRemovePaymentMethodClicked(item: PaymentMethodEntity) {
    }
}