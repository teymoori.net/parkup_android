package app.parkup.presentation.ui.drawer

data class DrawerItemEntity
    (
    val title: String,
    val icon: Int,
    val type: DrawerItemType
)

enum class DrawerItemType {
    PAYMENT_METHODS, HISTORY, VEHICLES, TERMS, HELP,LOGOUT , SETTING
}