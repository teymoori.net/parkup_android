package app.parkup.presentation.ui.orders

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.*
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class OrdersViewModel @Inject constructor(
    private val getOrders: GetOrders

) : BaseViewModel() {

    val ordersState: MutableLiveData<Response<List<OrderEntity>>> = MutableLiveData()


    override fun onViewResumed() {
        super.onViewResumed()
        getOrders()
    }

    private fun getOrders() {
        ordersState.value = Loading(true)

        compositeDisposable.add(
            getOrders.observable().subscribe({
                ordersState.value = Success(it)
            },
                {
                    ordersState.value = ErrorIn(it.message)
                })
        )
    }



}