package app.parkup.presentation.ui.vehicles

import androidx.lifecycle.MutableLiveData
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.*
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.log
import javax.inject.Inject

class VehiclesViewModel @Inject constructor(
    private val getVehicles: GetVehicles,
    private val removeVehicle: RemoveVehicle,
    private val setDefaultVehicleCase: SetDefaultVehicle

) : BaseViewModel() {

    val vehiclesState: MutableLiveData<Response<List<VehicleEntity>>> = MutableLiveData()
    var removeVehicleState: MutableLiveData<Response<Any>> = MutableLiveData()


    override fun onViewResumed() {
        super.onViewResumed()
        getVehicles()
    }

    private fun getVehicles() {
        vehiclesState.value = Loading(true)

        compositeDisposable.add(
            getVehicles.observable().subscribe({
                val data: MutableList<VehicleEntity> = mutableListOf()
                data.addAll(it)
                vehiclesState.value = Success(data as List<VehicleEntity>)
            },
                {
                    vehiclesState.value = ErrorIn(it.message)
                })
        )
    }

    fun removeVehicle(vehicleEntity: VehicleEntity?) {
        if (vehicleEntity != null) {
            removeVehicleState.value = Loading(true)
            compositeDisposable.add(
                removeVehicle.removeVehicle(
                    vehicleEntity
                ).subscribe({
                    getVehicles()
                    removeVehicleState.value = Success(null)

                },
                    {
                        getVehicles()
                        removeVehicleState.value = ErrorIn(message = it.message)
                    })
            )
        }
    }

    fun setDefaultVehicle(vehicleEntity: VehicleEntity?) {
        if (vehicleEntity != null) {
            compositeDisposable.add(
                setDefaultVehicleCase.setDefaultVehicle(
                    vehicleEntity
                ).subscribe({
                    getVehicles()
                },
                    {
                        getVehicles()
                        removeVehicleState.value = ErrorIn(message = it.message)
                    })
            )
        }
    }

}