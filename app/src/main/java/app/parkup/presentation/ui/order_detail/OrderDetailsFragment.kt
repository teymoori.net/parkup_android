package app.parkup.presentation.ui.order_detail

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.domain.entities.KeyValueEntity
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.OrderEntity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.tools.AppUtils.Companion.viewToBitmapAndShare
import com.pixabay.utils.tools.LocationUtils
import com.pixabay.utils.tools.QrCodeUtils
import com.pixabay.utils.tools.setImageDrawable
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_order_details.*

import java.util.*
import javax.inject.Inject


class OrderDetailsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var adapter: KeyValueListAdapter

    private lateinit var viewModel: OrderDetailViewModel

    var orderID: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            orderID = (it.getString("orderid") ?: "6").toInt()
            //orderID = it.getInt(Cons.ITEM_BUNDLE)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getOrder(orderID)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(OrderDetailViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }
        pageTitle.text = getString(R.string.receipt)

        viewModel.orderState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> showData(it.data)
                is Loading -> loading(true)
                is ErrorIn -> showError(msg = it.message)
            }
        })

        pay.setOnClickListener {
            Navigation.findNavController(it).navigate(
                R.id.action_orderDetailsFragment_to_paymentDialogFragment,
                bundleOf(Cons.ITEM_BUNDLE to orderID)
            )
        }

    }

    @SuppressLint("SetTextI18n")
    private fun showData(order: OrderEntity?) {
        loading(false)

        qrCode.setImageBitmap(QrCodeUtils.createQR(AppUtils.getOrderDeepLinkUri(order?.id)))
        orderCode.text = "#${order?.order_code}"
        cost.text = "$${order?.cost}"

        val details = mutableListOf<KeyValueEntity>()

        details.add(
            KeyValueEntity(
                key = "Status",
                value = order?.order_status?.capitalize()
            )
        )

        details.add(
            KeyValueEntity(
                key = "Customer",
                value = order?.customer_name
            )
        )
        details.add(
            KeyValueEntity(
                key = "Customer Phone",
                value = order?.customer_phone
            )
        )

        details.add(
            KeyValueEntity(
                key = "Parking",
                value = order?.parking?.full_address
            )
        )
        details.add(
            KeyValueEntity(
                key = "Vehicle",
                value = "${order?.vehicle?.model}  #${order?.vehicle?.plate_number}"
            )
        )
        details.add(
            KeyValueEntity(
                key = "Park Time",
                value = "${order?.check_in_time} \nto\n${order?.check_out_time}\n( ${order?.duration_text} )"
            )
        )

        adapter.items = details
        detailsList.adapter = adapter

        //pay btn
        if (order?.order_status == "paid" || order?.order_status == "done") {
            pay.visibility = View.GONE
            navigate.visibility = View.VISIBLE


            orderCode.visibility = View.VISIBLE
            qrCode.visibility = View.VISIBLE
            payStatusIcon.setImageDrawable(activity, R.drawable.ic_circle_green_tick)
        } else {
            pay.visibility = View.VISIBLE
            navigate.visibility = View.GONE
            orderCode.visibility = View.GONE
            qrCode.visibility = View.GONE
            payStatusIcon.setImageDrawable(activity, R.drawable.ic_circle_red_close)
        }

        navigate.setOnClickListener {
            LocationUtils.navigate(
                activity,
                order?.parking?.title,
                order?.parking?.latitude,
                order?.parking?.longitude
            )
        }

        share.setOnClickListener { shareBill(receiptContainer) }
    }

    private fun shareBill(view:View) {
        Dexter.withContext(activity)
            .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    viewToBitmapAndShare(view, activity)
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                    showError(true , "ParkUp needs storage permission to share the receipt file")
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    showError(true , "ParkUp needs storage permission to share the receipt file")
                }
            }).check()
    }

//    private fun showError(message: String?) {
//        loading(false)
//        message?.log("order_00")?.toast(activity)
//    }

}
