package app.parkup.presentation.ui.scan_card

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardDetailsEntity(
    var cardNumber: String? = null,
    var month: String? = null,
    var year: String? = null
) : Parcelable