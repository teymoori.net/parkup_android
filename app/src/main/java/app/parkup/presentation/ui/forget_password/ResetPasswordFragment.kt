package app.parkup.presentation.ui.forget_password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import app.parkup.R
import app.parkup.data.entities.ResetPasswordRequestData
import app.parkup.domain.entities.MessageEntity
import app.parkup.utils.tools.Cons
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_reset_password.*
import kotlinx.android.synthetic.main.fragment_reset_password.resetPassword
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class ResetPasswordFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ForgetPasswordViewModel

    var hash: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        arguments?.let {
            hash = it.getString(Cons.ITEM_BUNDLE) ?: ""
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_reset_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ForgetPasswordViewModel::class.java)

        back.setOnClickListener { activity?.onBackPressed() }

        resetPassword.setOnClickListener {
            viewModel.resetPassword(
                ResetPasswordRequestData(
                    otp = otp.text.toString(),
                    password = password.text.toString(),
                    hash = hash
                )
            )
        }

        viewModel.changePassword.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> showData(it.data)
                is Loading -> resetPassword.showLoading(true)
                is ErrorIn -> showError(msg = it.message)
            }
        })
    }

    private fun showData(data: MessageEntity?) {
        Navigation.findNavController(resetPassword).navigate(
            R.id.action_resetPasswordFragment_to_loginFragment
        )
        resetPassword.showLoading(false)
        showError(isError = false, msg = data?.message)
    }
}