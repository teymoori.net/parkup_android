package app.parkup.presentation.ui.check_vehicle

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import app.parkup.R
import app.parkup.presentation.ui.orders.OrdersAdapter
import app.parkup.utils.tools.OrderEntity
import com.jakewharton.rxbinding2.widget.RxTextView
import com.pixabay.utils.base.BaseFragment
import com.pixabay.utils.models.ErrorIn
import com.pixabay.utils.models.Loading
import com.pixabay.utils.models.Success
import com.pixabay.utils.tools.AppUtils
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_check_vehicle.*
import kotlinx.android.synthetic.main.fragment_check_vehicle.pageTitle
import kotlinx.android.synthetic.main.fragment_check_vehicle.scan
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CheckVehicleFragment : BaseFragment(), OrdersAdapter.ClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CheckVehicleViewModel

    @Inject
    lateinit var ordersAdapter: OrdersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_check_vehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CheckVehicleViewModel::class.java)

        pageTitle.text = getString(R.string.check_vehicle)

        back.setOnClickListener { activity?.onBackPressed() }

        viewModel.search("")

        compositeDisposable.add(
            RxTextView.textChanges(word)
                .skip(2)
                .debounce(700, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewModel.search(it.toString())
                }, {})
        )

        viewModel.searchState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Success -> showOrders(it.data)
                is Loading -> loading(it.isLoading)
                is ErrorIn -> showError(it.message)
            }
        })

        orders.adapter = ordersAdapter
        orders.setOnItemClickListener { adapter, _, pos, _ ->
            onOrderClicked(
                adapter.getItemAtPosition(
                    pos
                ) as? OrderEntity
            )
        }

        scan.setOnClickListener {
            Navigation.findNavController(pageTitle).navigate(
                R.id.action_checkVehicleFragment_to_licencePlateScannerFragment
            )
        }
    }

    private fun showOrders(orders: List<OrderEntity>?) {
        loading(false)
        ordersAdapter.items = orders
    }

    private fun showError(msg: String?) {
        loading(false)
    }

    override fun onOrderClicked(item: OrderEntity?) {
        NavHostFragment.findNavController(this).navigate(
            Uri.parse(AppUtils.getOrderDeepLinkUri(item?.id))
        )
    }
}