package app.parkup.presentation.ui.check_order

import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.AddOrderRequestData
import app.parkup.domain.entities.ParkingEntity
import app.parkup.domain.entities.PaymentEntity
import app.parkup.domain.entities.PaymentMethodEntity
import app.parkup.domain.entities.VehicleEntity
import app.parkup.domain.use_cases.*
import app.parkup.utils.tools.OrderEntity
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import com.pixabay.utils.tools.ifLet
import com.pixabay.utils.tools.log
import javax.inject.Inject
import kotlin.math.cos

class CheckOrderViewModel @Inject constructor(
    private val vehicles: GetVehicles,
    private val paymentMethods: GetPaymentMethods,
    private val parkings: GetParking,
    private val addOrder: AddOrder,
    private val pay: Pay

) : BaseViewModel() {


    var parking: ParkingEntity? = null
    var paymentMethod: PaymentMethodEntity? = null
    var vehicle: VehicleEntity? = null


    val parkingViewResult: MutableLiveData<ParkingEntity> = MutableLiveData()
    val vehicleViewResult: MutableLiveData<VehicleEntity> = MutableLiveData()
    val paymentMethodViewResult: MutableLiveData<PaymentMethodEntity> = MutableLiveData()
    val payState: MutableLiveData<Response<Int>> = MutableLiveData()

    val error: MutableLiveData<String?> = MutableLiveData()

    fun getData(parkingId: Int) {
        getParking(parkingId)
        getVehicle()
        getPaymentMethod()
    }


    fun getParking(parkingID: Int) {
        compositeDisposable.add(
            parkings.getParking(parkingID).subscribe({
                parking = it
                parkingViewResult.postValue(it)
            },
                {
                    // parkingViewResult.postValue(it.message)
                })
        )
    }

    fun getVehicle() {
        compositeDisposable.add(
            vehicles.observable().subscribe({
                val defaultVehicle = it.first { it.default_vehicle == true } ?: null
                vehicleViewResult.postValue(defaultVehicle)
                vehicle = defaultVehicle
            },
                {
                    vehicleViewResult.postValue(null)
                    vehicle = null
                })
        )
    }

    fun getPaymentMethod() {
        compositeDisposable.add(
            paymentMethods.observable().subscribe({
                val defaultMethod = it.first { it.is_default } ?: null
                paymentMethodViewResult.postValue(defaultMethod)
                paymentMethod = defaultMethod
            },
                {
                    paymentMethodViewResult.postValue(null)
                    paymentMethod = null
                })
        )
    }


    fun pay(cost: Int?, hours: Int?) {
        if (parking == null) {
            error.postValue("Parking is empty")
            return
        }
        if (paymentMethod == null) {
            error.postValue("Payment Method is empty")
            return
        }
        if (vehicle == null) {
            error.postValue("Vehicle is empty")
            return
        }

        addOrder(parking = parking, vehicle = vehicle, cost = cost, hours = hours)

    }


    private fun addOrder(
        parking: ParkingEntity?,
        vehicle: VehicleEntity?,
        cost: Int?,
        hours: Int?
    ) {
        payState.value = Loading(true)
        compositeDisposable.add(
            addOrder.addOrder(
                AddOrderRequestData(
                    vehicleID = vehicle?.id,
                    parkingID = parking?.id,
                    hours = hours,
                    cost = cost
                )
            ).subscribe({
                if (it != null)
                    payOrder(it.id)
            }, {
            }
            )
        )
    }


    private fun payOrder(orderId: Int?) {
        compositeDisposable.add(
            pay.pay(orderId?.toLong() ?: 0).subscribe({
                if (it.status == "succeeded") {
                    payState.value = Success(orderId)
                } else {
                    payState.value = ErrorIn(it.cancellation_reason)
                }
            },
                {
                })
        )


    }


}