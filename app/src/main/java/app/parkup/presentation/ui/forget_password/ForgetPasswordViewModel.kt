package app.parkup.presentation.ui.forget_password

import androidx.lifecycle.MutableLiveData
import app.parkup.data.entities.ResetPasswordRequestData
import app.parkup.domain.entities.ForgetPasswordEntity
import app.parkup.domain.entities.MessageEntity
import app.parkup.domain.entities.UserEntity
import app.parkup.domain.use_cases.ForgetPassword
import app.parkup.domain.use_cases.ResetPassword
import app.parkup.utils.tools.OrderEntity
import com.onesignal.OneSignal
import com.pixabay.utils.base.BaseViewModel
import com.pixabay.utils.models.*
import javax.inject.Inject

class ForgetPasswordViewModel @Inject constructor(
    private val forgetPassword: ForgetPassword,
    private val resetPassword: ResetPassword

) : BaseViewModel() {

    val requestCode: MutableLiveData<Response<ForgetPasswordEntity>> = MutableLiveData()
    val changePassword: MutableLiveData<Response<MessageEntity>> = MutableLiveData()

    fun requestCode(username: String?) {
        if (username.isNullOrEmpty()) {
            requestCode.value = ErrorIn("Username/Password is empty.")
        } else {
            requestCode.value = Loading(true)
            compositeDisposable.add(

                forgetPassword.requestCode(username).subscribe({

                    requestCode.value = Success(it)

                },
                    {
                        requestCode.value = ErrorIn(it.message)
                    })

            )
        }
    }

    fun resetPassword(request: ResetPasswordRequestData?) {
        if (request == null) {
            changePassword.postValue(ErrorIn("fields is empty."))
        } else {
            changePassword.postValue(Loading(true))
            compositeDisposable.add(

                resetPassword.resetPassword(request).subscribe({

                    changePassword.postValue(Success(it))

                },
                    {
                        requestCode.postValue(ErrorIn(it.message))
                    })

            )
        }
    }

}