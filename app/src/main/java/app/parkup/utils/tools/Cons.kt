package app.parkup.utils.tools

class Cons {

    companion object {
        val BASE_URL = "https://parkup.app/parkup/public/api/"
        const val ITEM_BUNDLE = "item"
        const val ITEM_PRICE = "price"
        const val CAN_SKIP = "can_skip"
        const val SHOW_PAY = "show_pay"
    }
}