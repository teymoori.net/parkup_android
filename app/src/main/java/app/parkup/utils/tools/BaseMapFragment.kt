package app.parkup.utils.tools

import android.app.Activity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import app.parkup.R
import com.google.maps.android.ui.IconGenerator

open class BaseMapFragment : Fragment() {

    lateinit var mActivity: Activity
    lateinit var iconGen: IconGenerator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity as Activity

        iconGen = IconGenerator(activity);
        iconGen.setBackground(
            ContextCompat.getDrawable(
                mActivity,
                R.drawable.ic_parking_marker_40
            )
        )
    }
}


