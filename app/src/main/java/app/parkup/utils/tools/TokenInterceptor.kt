package app.parkup.utils.tools

import app.parkup.data.repositories.login.UserCacheImpl
import app.parkup.domain.entities.UserEntity
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class TokenInterceptor @Inject constructor(
    private val dataProvider: DataProvider
) : Interceptor {

    var token: String? = ""

    @Inject
    lateinit var userCacheImpl: UserCacheImpl

    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()

        if (request.header("No-Authentication") == null) {
            val user = dataProvider.getData(UserCacheImpl.USER_KEY) as? UserEntity
            token = user?.access_token
            val finalToken = "Bearer $token"
            request = request.newBuilder()
                .addHeader("Authorization", finalToken)
                .build()
        }
        return chain.proceed(request)
    }

}