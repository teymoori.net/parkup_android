package app.parkup.utils.tools

class ColorEntity(
    val color: String,
    val rgb: String,
    var selected: Boolean? = false
)