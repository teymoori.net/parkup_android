package app.parkup.utils.tools

import app.parkup.utils.di.components.DaggerApplicationComponent
import com.onesignal.OneSignal
import com.orhanobut.hawk.Hawk
import com.stripe.android.PaymentConfiguration
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MyApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerApplicationComponent.builder().application(this).build()
        component.inject(this)
        return component
    }

    override fun onCreate() {
        super.onCreate()
        Hawk.init(this).build()
        oneSignalInit()

        PaymentConfiguration.init(this, "pk_test_qRd9shBOXYBCYPCcLVKnB5yU00aDGYBevV");


    }

    private fun oneSignalInit(){
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            //.unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}