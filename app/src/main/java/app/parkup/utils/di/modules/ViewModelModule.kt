package app.parkup.utils.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.parkup.presentation.ui.add_payment_method.AddPaymentMethodsViewModel
import app.parkup.presentation.ui.add_vehicle.AddVehicleViewModel
import app.parkup.presentation.ui.check_order.CheckOrderViewModel
import app.parkup.presentation.ui.check_vehicle.CheckVehicleViewModel
import app.parkup.presentation.ui.dashboard.DashboardViewModel
import app.parkup.presentation.ui.drawer.DrawerViewModel
import app.parkup.presentation.ui.forget_password.ForgetPasswordViewModel
import app.parkup.presentation.ui.login.LoginViewModel
import app.parkup.presentation.ui.order_detail.OrderDetailViewModel
import app.parkup.presentation.ui.orders.OrdersViewModel
import app.parkup.presentation.ui.parking_detail.ParkingDetailsViewModel
import app.parkup.presentation.ui.parking_details_dialog.PaymentDialogViewModel
import app.parkup.presentation.ui.payment_methods.PaymentMethodsViewModel
import app.parkup.presentation.ui.register.RegisterViewModel
import app.parkup.presentation.ui.settings.SettingsViewModel
import app.parkup.presentation.ui.splash.SplashViewModel
import app.parkup.presentation.ui.terms.TermsConditionsViewModel
import app.parkup.presentation.ui.vehicles.VehiclesViewModel
import dagger.Binds
import com.pixabay.utils.di.ViewModelFactory
import com.pixabay.utils.di.ViewModelKey
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    internal abstract fun registerVM(viewModel: RegisterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun loginVM(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    internal abstract fun dashboardVM(viewModel: DashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DrawerViewModel::class)
    internal abstract fun drawerVM(viewModel: DrawerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun splashVM(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ParkingDetailsViewModel::class)
    internal abstract fun parkingDetailVM(viewModel: ParkingDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VehiclesViewModel::class)
    internal abstract fun vehiclesVM(viewModel: VehiclesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymentMethodsViewModel::class)
    internal abstract fun pmsVM(viewModel: PaymentMethodsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddVehicleViewModel::class)
    internal abstract fun addVehicleVM(viewModel: AddVehicleViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(OrderDetailViewModel::class)
    internal abstract fun orderDetail(viewModel: OrderDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PaymentDialogViewModel::class)
    internal abstract fun paymentDetail(viewModel: PaymentDialogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrdersViewModel::class)
    internal abstract fun ordersVM(viewModel: OrdersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun settingsVM(viewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckVehicleViewModel::class)
    internal abstract fun checkVehicle(viewModel: CheckVehicleViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckOrderViewModel::class)
    internal abstract fun checkOrder(viewModel: CheckOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgetPasswordViewModel::class)
    internal abstract fun forgetPass(viewModel: ForgetPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TermsConditionsViewModel::class)
    internal abstract fun terms(viewModel: TermsConditionsViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(AddPaymentMethodsViewModel::class)
    internal abstract fun addPm(viewModel: AddPaymentMethodsViewModel): ViewModel


}