package app.parkup.utils.di.modules

import app.parkup.data.api.Api
import app.parkup.data.repositories.forget_password.ForgetPasswordImpl
import app.parkup.data.repositories.login.UserCacheImpl
import app.parkup.data.repositories.get_parkings.GetParkingsRemoteRepository
import app.parkup.data.repositories.get_parkings.GetParkingsRepositoryImpl
import app.parkup.data.repositories.login.UserLoginRemoteRepository
import app.parkup.data.repositories.login.UserLoginRepositoryImpl
import app.parkup.data.repositories.orders.OrdersRemoteRepository
import app.parkup.data.repositories.orders.OrdersRepositoryImpl
import app.parkup.data.repositories.payment.PaymentRemoteRepository
import app.parkup.data.repositories.payment.PaymentRepositoryImpl
import app.parkup.data.repositories.payment_methods.PaymentMethodsRemoteRepository
import app.parkup.data.repositories.payment_methods.PaymentMethodsRepositoryImpl
import app.parkup.data.repositories.register.UserRegisterRemoteRepository
import app.parkup.data.repositories.register.UserRegisterRepositoryImpl
import app.parkup.data.repositories.stripe_secret.StripeSecretRemoteRepository
import app.parkup.data.repositories.stripe_secret.StripeSecretRepositoryImpl
import app.parkup.data.repositories.terms.TermsConditionsRepositoryImpl
import app.parkup.data.repositories.user_details.UserDetailsRemoteRepository
import app.parkup.data.repositories.user_details.UserDetailsRepositoryImpl
import app.parkup.data.repositories.vehicles.VehiclesRemoteRepository
import app.parkup.data.repositories.vehicles.VehiclesRepositoryImpl
import app.parkup.domain.interactors.forget_password.ForgetPasswordRepository
import app.parkup.domain.interactors.payment_methods.PaymentRepository
import app.parkup.domain.interactors.get_parkings.GetParkingsRepository
import app.parkup.domain.interactors.order.OrdersRepository
import app.parkup.domain.interactors.payment_methods.PaymentMethodsRepository
import app.parkup.domain.interactors.stripe_secret.StripeSecretRepository
import app.parkup.domain.interactors.terms.TermsConditionsRepository
import app.parkup.domain.interactors.user_details.UserDetailsRepository
import app.parkup.domain.interactors.user_login.UserLoginRepository
import app.parkup.domain.interactors.user_register.UserRegisterRepository
import app.parkup.domain.interactors.vehicles.VehiclesRepository
import app.parkup.domain.use_cases.*
import app.parkup.utils.tools.AsyncTransformer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class DataModule {

    @Singleton
    @Provides
    fun terMConditionsRepo(api: Api): TermsConditionsRepository {
        return TermsConditionsRepositoryImpl(
            api
        )
    }

    @Singleton
    @Provides
    fun provideForgetPAssRepo(api: Api): ForgetPasswordRepository {
        return ForgetPasswordImpl(
            api
        )
    }

    @Singleton
    @Provides
    fun providePaymentRepo(api: Api): PaymentRepository {
        return PaymentRepositoryImpl(
            PaymentRemoteRepository(
                api
            )
        )
    }

    @Singleton
    @Provides
    fun provideOrderRepo(api: Api): OrdersRepository {
        return OrdersRepositoryImpl(
            OrdersRemoteRepository(
                api
            )
        )
    }

    @Singleton
    @Provides
    fun provideStripeSecretsRepo(api: Api): StripeSecretRepository {
        return StripeSecretRepositoryImpl(
            StripeSecretRemoteRepository(
                api
            )
        )
    }

    @Singleton
    @Provides
    fun providePaymentMethodsRepo(api: Api): PaymentMethodsRepository {
        return PaymentMethodsRepositoryImpl(
            PaymentMethodsRemoteRepository(
                api
            )
        )
    }

    @Singleton
    @Provides
    fun provideRegisterRepo(api: Api, userCacheImpl: UserCacheImpl): UserRegisterRepository {
        return UserRegisterRepositoryImpl(
            UserRegisterRemoteRepository(
                api
            ),
            userCacheImpl
        )
    }

    @Singleton
    @Provides
    fun provideUserDetailsRepo(api: Api): UserDetailsRepository {
        return UserDetailsRepositoryImpl(
            UserDetailsRemoteRepository(
                api
            )
        )
    }

    @Singleton
    @Provides
    fun provideLoginRepo(api: Api, userCacheImpl: UserCacheImpl): UserLoginRepository {
        return UserLoginRepositoryImpl(
            UserLoginRemoteRepository(
                api
            ),
            userCacheImpl
        )
    }

    @Singleton
    @Provides
    fun provideParkingsRepo(api: Api): GetParkingsRepository {
        return GetParkingsRepositoryImpl(
            GetParkingsRemoteRepository(
                api
            )
        )
    }

    @Singleton
    @Provides
    fun provideVehiclesRepo(api: Api): VehiclesRepository {
        return VehiclesRepositoryImpl(
            VehiclesRemoteRepository(
                api
            )
        )
    }


    @Singleton
    @Provides
    fun login(userLoginRepository: UserLoginRepository): LoginUser {
        return LoginUser(
            userLoginRepository = userLoginRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getParkings(parkingsRepository: GetParkingsRepository): GetParkings {
        return GetParkings(
            parkingsRepository = parkingsRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getParking(parkingsRepository: GetParkingsRepository): GetParking {
        return GetParking(
            parkingRepository = parkingsRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun register(registerRepository: UserRegisterRepository): RegisterUser {
        return RegisterUser(
            userRegisterRepository = registerRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getCachedUser(userCacheImpl: UserCacheImpl): GetCachedUser {
        return GetCachedUser(
            userCache = userCacheImpl,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun checkUserLoggedIn(userCacheImpl: UserCacheImpl): CheckUserIsLoggedIn {
        return CheckUserIsLoggedIn(
            userCache = userCacheImpl,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun logOutUser(userCacheImpl: UserCacheImpl): LogOutUser {
        return LogOutUser(
            userCache = userCacheImpl,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getUser(userDetailsRepository: UserDetailsRepository): GetUserDetails {
        return GetUserDetails(
            userDetailsRepository = userDetailsRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun addVehicle(vehiclesRepository: VehiclesRepository): AddVehicle {
        return AddVehicle(
            vehiclesRepository = vehiclesRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun setDefaultVehicle(vehiclesRepository: VehiclesRepository): SetDefaultVehicle {
        return SetDefaultVehicle(
            vehiclesRepository = vehiclesRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun removeVehicle(vehiclesRepository: VehiclesRepository): RemoveVehicle {
        return RemoveVehicle(
            vehiclesRepository = vehiclesRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getVehicles(vehiclesRepository: VehiclesRepository): GetVehicles {
        return GetVehicles(
            vehiclesRepository = vehiclesRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getPaymentMethods(paymentMethodsRepository: PaymentMethodsRepository): GetPaymentMethods {
        return GetPaymentMethods(
            paymentMethodsRepository = paymentMethodsRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getStripeSecretsMethods(stripeSecretRepository: StripeSecretRepository): GetStripeSecrets {
        return GetStripeSecrets(
            stripeSecretRepository = stripeSecretRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun setDefaultPaymentMethod(paymentMethodsRepository: PaymentMethodsRepository): SetDefaultPaymentMethod {
        return SetDefaultPaymentMethod(
            paymentMethodsRepository = paymentMethodsRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun removePaymentMethod(paymentMethodsRepository: PaymentMethodsRepository): RemovePaymentMethod {
        return RemovePaymentMethod(
            paymentMethodsRepository = paymentMethodsRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun addNewOrder(ordersRepository: OrdersRepository): AddOrder {
        return AddOrder(
            ordersRepository = ordersRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getOrder(ordersRepository: OrdersRepository): GetOrder {
        return GetOrder(
            ordersRepository = ordersRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getOrders(ordersRepository: OrdersRepository): GetOrders {
        return GetOrders(
            ordersRepository = ordersRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun searchOrders(ordersRepository: OrdersRepository): SearchOrders {
        return SearchOrders(
            ordersRepository = ordersRepository,
            transformer = AsyncTransformer()
        )
    }


    @Singleton
    @Provides
    fun payment(paymentRepository: PaymentRepository): Pay {
        return Pay(
            paymentRepository = paymentRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun forgetPassword(forgetPasswordRepository: ForgetPasswordRepository): ForgetPassword {
        return ForgetPassword(
            forgetPasswordRepository = forgetPasswordRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun resetPassword(forgetPasswordRepository: ForgetPasswordRepository): ResetPassword {
        return ResetPassword(
            forgetPasswordRepository = forgetPasswordRepository,
            transformer = AsyncTransformer()
        )
    }

    @Singleton
    @Provides
    fun getTermsUseCase(termsConditionsRepository: TermsConditionsRepository): GetTermsConditions {
        return GetTermsConditions(
            termsConditionsRepository = termsConditionsRepository,
            transformer = AsyncTransformer()
        )
    }


}