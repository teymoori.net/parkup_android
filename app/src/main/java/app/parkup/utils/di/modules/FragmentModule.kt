package app.parkup.utils.di.modules

import app.parkup.presentation.ui.licence_plate_scan.LicencePlateScannerFragment
import app.parkup.presentation.ui.add_payment_method.AddPaymentMethodFragment
import app.parkup.presentation.ui.parking_price_list_dialog.ParkingPriceListDialogFragment
import app.parkup.presentation.ui.payment_dialog.PaymentDialogFragment
import app.parkup.presentation.ui.add_vehicle.AddVehicleFragment
import app.parkup.presentation.ui.check_order.CheckOrderFragment
import app.parkup.presentation.ui.check_vehicle.CheckVehicleFragment
import app.parkup.presentation.ui.drawer.NavigationDrawerFragment
import app.parkup.presentation.ui.parking_details_dialog.ParkingDetailDialogFragment
import app.parkup.presentation.ui.dashboard.DashboardFragment
import app.parkup.presentation.ui.forget_password.ForgetPasswordFragment
import app.parkup.presentation.ui.forget_password.ResetPasswordFragment
import app.parkup.presentation.ui.login.LoginFragment
import app.parkup.presentation.ui.order_detail.OrderDetailsFragment
import app.parkup.presentation.ui.orders.OrdersListFragment
import app.parkup.presentation.ui.parking_detail.ParkingDetailFragment
import app.parkup.presentation.ui.payment_methods.PaymentMethodsFragment
import app.parkup.presentation.ui.register.RegisterDefineVehicleFragment
import app.parkup.presentation.ui.register.RegisterGetMailPhoneFragment
import app.parkup.presentation.ui.register.RegisterGetUserPersonalDetailsFragment
import app.parkup.presentation.ui.settings.SettingsFragment
import app.parkup.presentation.ui.splash.SplashFragment
import app.parkup.presentation.ui.terms.TermsAndConditionsFragment
import app.parkup.presentation.ui.vehicles.VehiclesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun splashFragment(): SplashFragment

    @ContributesAndroidInjector
    internal abstract fun loginFragment(): LoginFragment

    @ContributesAndroidInjector
    internal abstract fun registerGetUserFragment(): RegisterGetUserPersonalDetailsFragment

    @ContributesAndroidInjector
    internal abstract fun registerGetPhoneMailFragment(): RegisterGetMailPhoneFragment

    @ContributesAndroidInjector
    internal abstract fun registerDefineVehicleFragment(): RegisterDefineVehicleFragment

    @ContributesAndroidInjector
    internal abstract fun dashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    internal abstract fun parkingDetailFragment(): ParkingDetailDialogFragment

    @ContributesAndroidInjector
    internal abstract fun drawerFragment(): NavigationDrawerFragment

    @ContributesAndroidInjector
    internal abstract fun parkingDetail(): ParkingDetailFragment

    @ContributesAndroidInjector
    internal abstract fun vehiclesDetail(): VehiclesFragment

    @ContributesAndroidInjector
    internal abstract fun pms(): PaymentMethodsFragment

    @ContributesAndroidInjector
    internal abstract fun addVehicle(): AddVehicleFragment

    @ContributesAndroidInjector
    internal abstract fun orderDetail(): OrderDetailsFragment

    @ContributesAndroidInjector
    internal abstract fun paymentDialog(): PaymentDialogFragment

    @ContributesAndroidInjector
    internal abstract fun ordersList(): OrdersListFragment

    @ContributesAndroidInjector
    internal abstract fun settings(): SettingsFragment

    @ContributesAndroidInjector
    internal abstract fun checkVehicle(): CheckVehicleFragment


    @ContributesAndroidInjector
    internal abstract fun checkOrder(): CheckOrderFragment

    @ContributesAndroidInjector
    internal abstract fun parkingPrices(): ParkingPriceListDialogFragment

    @ContributesAndroidInjector
    internal abstract fun forgetPass(): ForgetPasswordFragment

    @ContributesAndroidInjector
    internal abstract fun resetPass(): ResetPasswordFragment

    @ContributesAndroidInjector
    internal abstract fun termsConditions(): TermsAndConditionsFragment

    @ContributesAndroidInjector
    internal abstract fun addPmMethod(): AddPaymentMethodFragment

    @ContributesAndroidInjector
    internal abstract fun licencePlateScan(): LicencePlateScannerFragment


}