package app.parkup.utils.di.modules

import app.parkup.data.api.Api
import app.parkup.utils.tools.Cons
import app.parkup.utils.tools.TokenInterceptor
import com.pixabay.utils.di.RetrofitServiceGenerator
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
open class ApplicationModule {

    @Singleton
    @Provides
    fun provideRetrofit(
        converter: GsonConverterFactory,
        textConverter: ScalarsConverterFactory,
        httpClient: OkHttpClient.Builder,
        @Named("baseURL") baseURL: String
    ): Retrofit {
        val retrofitClass =
            RetrofitServiceGenerator(
                textConverter,
                converter,
                httpClient,
                baseURL
            )
        return retrofitClass.getClient()
    }

    @Singleton
    @Provides
    fun provideOkHttp( tokenInterceptor: TokenInterceptor
    ): OkHttpClient.Builder {
        val httpClient = OkHttpClient().newBuilder()
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.callTimeout(30, TimeUnit.SECONDS)
        httpClient.addInterceptor(tokenInterceptor)

        //body log
        val loggerInterceptorBody = HttpLoggingInterceptor()
        loggerInterceptorBody.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(loggerInterceptorBody)

        //headers log
        val loggerInterceptorHeader = HttpLoggingInterceptor()
        loggerInterceptorHeader.level = HttpLoggingInterceptor.Level.HEADERS
        httpClient.addInterceptor(loggerInterceptorHeader)

        return httpClient
    }


    @Singleton
    @Provides
    fun provideGSONConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }
    @Singleton
    @Provides
    fun provideTextConverterFactory(): ScalarsConverterFactory {
        return ScalarsConverterFactory.create()
    }

    @Singleton
    @Provides
    @Named("baseURL")
    fun provideBaseURL(): String {
        return Cons.BASE_URL
    }

    @Singleton
    @Provides
    fun getMainInterface(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

//    @Singleton
//    @Provides
//    fun provideVideoDetailsFragment(  ): VideoDetailsFragment {
//        return VideoDetailsFragment()
//    }
//    @Singleton
//    @Provides
//    fun provideFragmentPagerAdapter( videoDetailsFragment: VideoDetailsFragment): FragmentManager {
//        return videoDetailsFragment.childFragmentManager
//    }
//    @Singleton
//    @Provides
//    fun provideMoviePagerAdapter( fm:FragmentManager): MoviePagerAdapter {
//        return MoviePagerAdapter(fm)
//    }

}