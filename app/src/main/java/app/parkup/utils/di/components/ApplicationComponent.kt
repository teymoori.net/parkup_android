package app.parkup.utils.di.components

import android.app.Application
import app.parkup.utils.di.modules.*
import app.parkup.utils.di.modules.ViewModelModule
import app.parkup.utils.tools.MyApplication
import dagger.Component
import javax.inject.Singleton
import dagger.BindsInstance
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.DaggerApplication

@Singleton
@Component(
    modules = [
        ContextModule::class,
        ApplicationModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        ActivityModule::class,
        FragmentModule::class,
        DataModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<DaggerApplication> {

    fun inject(application: MyApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }
}