package com.pixabay.utils.base

enum class DurationType {
    HOURLY, DAILY, MONTHLY
}