package com.pixabay.utils.base

import io.reactivex.disposables.CompositeDisposable
import androidx.lifecycle.ViewModel
import java.lang.ref.WeakReference

abstract class BaseViewModel : ViewModel() {


    val compositeDisposable: CompositeDisposable = CompositeDisposable()


    open fun onViewCreated() {}
    open fun onViewResumed() {}

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }


}