package com.pixabay.utils.base

import com.pixabay.utils.tools.AppUtils
import com.pixabay.utils.views.ProgressDialog
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : androidx.fragment.app.Fragment() {
    val compositeDisposable = CompositeDisposable()

    fun loading(show: Boolean) {
        if (show)
            ProgressDialog.get().show(activity)
        else
            ProgressDialog.get().hide()
    }

    fun showError(isError: Boolean? = true, msg: String? = null) {
        loading(false)
        AppUtils.showAlert(activity, null, msg, isError ?: false)
    }

}