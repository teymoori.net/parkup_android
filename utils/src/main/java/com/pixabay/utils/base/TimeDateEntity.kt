package com.pixabay.utils.base

data class TimeDateEntity(
    val year:Int,
    val month:Int,
    val monthName:String,
    val day:Int,
    val weekDayName:String,
    val hour12:Int,
    val hour24:Int,
    val minutes:Int,
    val AM_MP:String
)