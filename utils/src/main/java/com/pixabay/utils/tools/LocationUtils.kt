package com.pixabay.utils.tools

import android.app.Activity
import android.content.Intent
import android.net.Uri
import java.lang.Exception
import java.util.*

class LocationUtils {

    companion object{

        fun navigate(activity: Activity?, addressTitle: String?, lat: Double?, lng: Double?) {
            val url = String.format(Locale.ENGLISH, "geo:0,0?q=") + Uri.encode(
                String.format(
                    "%s@%f,%f",
                    addressTitle,
                    lat,
                    lng
                ), "UTF-8"
            );
            val gmmIntentUri = Uri.parse(url)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            try {
                activity?.startActivity(mapIntent)
            } catch (e: Exception) {
                AppUtils.showAlert(activity, null, "You don't have any navigation app.", true)
            }
        }


    }
}