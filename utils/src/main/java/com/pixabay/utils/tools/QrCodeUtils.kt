package com.pixabay.utils.tools

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder

class QrCodeUtils {
    companion object {

        private const val WHITE = -0x000000
        private const val BLACK = -0xFFFFFF

        fun createQR(msg: String?, width: Int = 450, height: Int = 450): Bitmap? {
            val multiFormatWriter = MultiFormatWriter()
            return try {
                val bitMatrix =
                    multiFormatWriter.encode(msg ?: "", BarcodeFormat.QR_CODE, width, height)
                val barcodeEncoder = BarcodeEncoder();
                val bitmap =  createBitmap(bitMatrix)
                bitmap
            } catch (e: Exception) {
                null
            }
        }

        private fun createBitmap(matrix: BitMatrix): Bitmap? {
            val width = matrix.width
            val height = matrix.height
            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                val offset = y * width
                for (x in 0 until width) {
                    pixels[offset + x] =
                        if (matrix[x, y]) BLACK else  WHITE
                }
            }
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            return bitmap
        }
    }


}