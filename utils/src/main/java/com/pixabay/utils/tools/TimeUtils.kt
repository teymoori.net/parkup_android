package com.pixabay.utils.tools

import com.pixabay.utils.base.TimeDateEntity
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit

class TimeUtils {

    companion object {
        fun getCurrentDateTime(): TimeDateEntity {
            return TimeDateEntity(
                year = SimpleDateFormat("yyyy").format(Date()).toInt(),
                month = SimpleDateFormat("MM").format(Date()).toInt(),
                monthName = SimpleDateFormat("MMM").format(Date()),
                day = SimpleDateFormat("dd").format(Date()).toInt(),
                weekDayName = SimpleDateFormat("E").format(Date()),
                hour12 = SimpleDateFormat("h").format(Date()).toInt(),
                hour24 = SimpleDateFormat("H").format(Date()).toInt(),
                minutes = SimpleDateFormat("m").format(Date()).toInt(),
                AM_MP = SimpleDateFormat("a").format(Date()).toUpperCase()
            )
        }

        fun dateTimeStringToDate(dateTime:String? , format:String?="h:ma E, MMM dd"): String {
            val parser =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val formatter = SimpleDateFormat(format)
            val formattedDate = formatter.format(parser.parse(dateTime))

            return formattedDate
        }
        fun dateToFormat(date: Date, format: String): String {
            return SimpleDateFormat(format).format(date)
        }

        fun monthToName(month: Int): String {
            return when (month) {
                1 -> "January"
                2 -> "February"
                3 -> "March"
                4 -> "April"
                5 -> "May"
                6 -> "June"
                7 -> "July"
                8 -> "August"
                9 -> "September"
                10 -> "October"
                11 -> "November"
                12 -> "December"
                else -> month.toString()
            }
        }

        fun getDatesDistance(firstDate: Date, secondDate: Date): String {
            var different: Long = secondDate.time - firstDate.time

            val secondsInMilli: Long = 1000
            val minutesInMilli = secondsInMilli * 60
            val hoursInMilli = minutesInMilli * 60
            val daysInMilli = hoursInMilli * 24
            val monthInMilli = daysInMilli * 30

            val elapsedMonth = different / monthInMilli
            different %= monthInMilli

            val elapsedDays = different / daysInMilli
            different %= daysInMilli

            val elapsedHours = different / hoursInMilli
            different %= hoursInMilli

            val elapsedMinutes = different / minutesInMilli
            different %= minutesInMilli

            val elapsedSeconds = different / secondsInMilli

            var result = ""

            if (elapsedMonth > 0) {
                result += "$elapsedMonth Month, "
            }

            if (elapsedDays > 0) {
                result += "$elapsedDays Days "
            }
            if (elapsedHours > 0) {
                result += "$elapsedHours Hours "
            }
            if (elapsedMinutes > 0) {
                result += "$elapsedMinutes Mins"
            }
            return result
        }
    }
}
