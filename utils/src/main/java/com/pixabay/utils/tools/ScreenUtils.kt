package com.pixabay.utils.tools

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics


class ScreenUtils {

    companion object {


        fun getScreenWidth(activity: Activity?): Int {
            val displayMetrics = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }



          fun  dpToPx(dp:Int): Float =
              dp * Resources.getSystem().displayMetrics.density
    }
}