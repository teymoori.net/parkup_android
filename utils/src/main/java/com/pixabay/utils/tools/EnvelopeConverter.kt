package com.pixabay.utils.tools

import retrofit2.Converter

/**
 * This Converter implicate for whenever we have a structure
 * called Envelope to look for the next response body in structure
 * and convert it to whatever object needed in ApiService
 */

class EnvelopeConverter : Converter.Factory() {
//    override fun responseBodyConverter(
//        type: Type,
//        annotations: Array<Annotation>,
//        retrofit: Retrofit
//    ): Converter<ResponseBody, *>? {
//        val envelopedType = TypeToken.getParameterized(Envelope::class.java, type).type
//        val delegate: Converter<ResponseBody, Envelope<Any>>? =
//            retrofit.nextResponseBodyConverter(this, envelopedType, annotations)
//        return Unwrap(delegate)
//    }
//
//    private class Unwrap<T>(
//        private val delegate: Converter<ResponseBody, Envelope<T>>?
//    ) : Converter<ResponseBody, T> {
//
//        override fun convert(value: ResponseBody): T? {
//            return delegate?.convert(value).data
//        }
//    }
}