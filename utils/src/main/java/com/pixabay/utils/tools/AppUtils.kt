package com.pixabay.utils.tools

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import com.pixabay.utils.R
import com.tapadoo.alerter.Alerter
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.lang.Exception
import java.util.*


class AppUtils {


    companion object {

        fun shareToMessagingApps(mActivity: Activity?, title: String, message: String) {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, message)
            intent.type = "text/plain"
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
            mActivity?.startActivity(Intent.createChooser(intent, title))
        }


        fun appVersion(ctx: Context?): String? {
            return try {
                val pInfo = ctx?.packageManager?.getPackageInfo(ctx.packageName, 0)
                "Version : ${pInfo?.versionName}"
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
                "000"
            }
        }


        fun assetsToString(context: Context?, file: String): String? {
            var json: String? = null
            try {
                val inputStream: InputStream? = context?.assets?.open(file)
                if (inputStream != null) {

                    val size: Int = inputStream.available()
                    val buffer = ByteArray(size)
                    inputStream.read(buffer)
                    inputStream.close()
                    json = String(buffer, Charsets.UTF_8)
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }
            return json
        }

        fun openGooglePlay(context: Context?) {
            val uri: Uri = Uri.parse("market://details?id=" + context?.packageName)
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK
            )
            try {
                context?.startActivity(goToMarket)
            } catch (e: ActivityNotFoundException) {
                context?.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + context?.getPackageName())
                    )
                )
            }
        }

        fun showAlert(
            mActivity: Activity?,
            title: String? = "",
            msg: String?,
            isError: Boolean = true
        ) {
            if (mActivity != null)
                Alerter.create(mActivity)
                    .setTitle(title ?: (if (isError) "Error" else ""))
                    .setText(msg ?: "")
                    .setDuration(8000)
                    .setBackgroundColorRes((if (isError) R.color.md_orange_700 else R.color.md_cyan_500)) // or setBackgroundColorInt(Color.CYAN)
                    .show()
        }


        fun viewToBitmap(
            context: Context?,
            activity: Activity?,
            layout: Int
        ): Bitmap? {

            val inflater = activity?.layoutInflater
            val view: View = inflater?.inflate(layout, null) ?: return null

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            view.setLayoutParams(
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            )
            view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            view.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(
                view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            view.draw(canvas)
            return bitmap
        }

        fun isColorDark(color: Int): Boolean {
            return ColorUtils.calculateLuminance(color) < 0.5;
        }


        fun viewToBitmapAndShare(view: View, mActivity: Activity?) {
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            val bgDrawable = view.background
            if (bgDrawable != null)
                bgDrawable.draw(canvas)
            else
                canvas.drawColor(Color.WHITE)
            view.draw(canvas)
            val share = Intent(Intent.ACTION_SEND)
            share.type = "image/jpeg"
            val values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, "title")
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            val uri = mActivity?.contentResolver?.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )
            val outStream: OutputStream?
            try {
                outStream = mActivity?.contentResolver?.openOutputStream(uri!!)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
                outStream!!.close()
            } catch (e: Exception) {
                System.err.println(e.toString())
            }
            share.putExtra(Intent.EXTRA_STREAM, uri)
            mActivity?.startActivity(Intent.createChooser(share, "Share Image"))
        }


        fun getParkingDeepLinkUri(parkingId: Int?) = "pk://a.a/?parkingid=$parkingId"
        fun getOrderDeepLinkUri(orderId: Int?) = "pk://b.b/?orderid=$orderId"


    }
}