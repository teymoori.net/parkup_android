package com.pixabay.utils

enum class PaymentMethods {
    VISA, MASTER, AMERICAN_EXPRESS, UNION_PAY
}