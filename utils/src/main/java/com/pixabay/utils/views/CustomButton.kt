package com.pixabay.utils.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color

import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.pixabay.utils.R
import kotlinx.android.synthetic.main.custom_button.view.*


class CustomButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleRes: Int = 0
) : ConstraintLayout(context, attrs, defStyleRes) {

    private var typedArray: TypedArray? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_button, this, true)

        if (!isInEditMode) {
            attrs?.let {
                typedArray = context.obtainStyledAttributes(it, R.styleable.CustomButton, 0, 0)

                showLoading(
                    typedArray?.getBoolean(R.styleable.CustomButton_button_show_loading, false)
                        ?: false
                )
                setText(typedArray?.getString(R.styleable.CustomButton_button_text) ?: "button")

                if (typedArray?.getBoolean(
                        R.styleable.CustomButton_justWithBorder,
                        false
                    ) == true
                ) {
                    bgView.setBackgroundResource(R.drawable.black_bordered)
                    button.setTextColor(Color.BLACK)
                } else {
                    bgView.setBackgroundResource(R.drawable.black_filled)
                    button.setTextColor(Color.WHITE)
                }

                typedArray?.recycle()
            }
        }

    }

    fun showLoading(showLoading: Boolean) {
        if (showLoading) {
            button.visibility = View.GONE
            loading.visibility = View.VISIBLE
        } else {
            button.visibility = View.VISIBLE
            loading.visibility = View.GONE
        }
    }

    fun setText(text: String) {
        button.text = text.capitalize()
    }

}