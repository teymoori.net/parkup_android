package com.pixabay.utils.views.spinner

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.pixabay.utils.R
import kotlinx.android.synthetic.main.custom_spinner_view.view.*
import my.mobilityone.onecent.ui.spinner.SpinnerDialogFragment

class CustomSpinnerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleRes: Int = 0
) : ConstraintLayout(context, attrs, defStyleRes), SpinnerDialogFragment.ReturnValue {

    private var typedArray: TypedArray? = null

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_spinner_view, this, true)

        container.setOnClickListener {
            openSpinnerDialog()
        }
        spinner.setHintTextColor(Color.BLACK)
        attrs?.let {
            typedArray = context.obtainStyledAttributes(it, R.styleable.CustomSpinnerView, 0, 0)
            setHint(
                typedArray?.getString(R.styleable.CustomSpinnerView_spinnerTitle)
                    ?: ""
            )
        }
    }

    var items: List<SpinnerDialogFragment.SpinnerDataModel> = emptyList()
        set(value) {
            // onItemSelected(value[0])
            field = value
        }
    var selectedItem: SpinnerDialogFragment.SpinnerDataModel? = null

    private fun openSpinnerDialog() {

        val spinnerFragment = SpinnerDialogFragment.instance.also {
            it.items = items as MutableList<SpinnerDialogFragment.SpinnerDataModel>
            it.onItemSelected = this
        }
        if (!spinnerFragment.isAdded)
            spinnerFragment.show(
                (context as AppCompatActivity).supportFragmentManager,
                SpinnerDialogFragment.tag
            )
    }

    override fun onItemSelected(value: SpinnerDialogFragment.SpinnerDataModel) {
        spinner.setText(value.title)
        spinner.isEnabled = false
        selectedItem = value
    }

    fun setHint(hint: String) {
        spinnerContainer.hint = hint
    }


}