package my.mobilityone.onecent.ui.spinner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.fragment.app.DialogFragment
import com.jakewharton.rxbinding2.widget.RxTextView
import com.pixabay.utils.R
import com.pixabay.utils.views.MyTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.spinner_dialog.*
import java.util.concurrent.TimeUnit

class SpinnerDialogFragment : DialogFragment() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    var adapter = ListAdapter()
    var items: MutableList<SpinnerDataModel> = mutableListOf<SpinnerDataModel>()

    companion object {
        var instance = SpinnerDialogFragment()
        val tag = System.currentTimeMillis().toString()
    }

    var selectedItem: SpinnerDataModel? = null

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setDimAmount(0.2F)
        return View.inflate(context, R.layout.spinner_dialog, null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    lateinit var onItemSelected: ReturnValue

    interface ReturnValue {
        fun onItemSelected(value: SpinnerDataModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        compositeDisposable.add(RxTextView.textChanges(search)
                .skip(1)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    search(it.toString())
                }, {}))


        list.adapter = adapter.also { it.items = items }

        list.setOnItemClickListener { _, _, position, _ ->
            if (::onItemSelected.isInitialized)
                onItemSelected.onItemSelected(adapter.items[position])
            dialog?.dismiss()
        }
    }

    private fun search(word: String) {
        adapter.items = items.filter { it.title.contains(word.trim(), true) }
    }

    class ListAdapter : BaseAdapter() {
        var items: List<SpinnerDataModel> = emptyList()
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun getView(pos: Int, convertView: View?, parent: ViewGroup?): View {
            val view: View
            val vh: Holder
            if (convertView == null) {
                view = LayoutInflater.from(parent?.context).inflate(R.layout.spinner_item, parent, false)
                vh = Holder(view)
                view?.tag = vh
            } else {
                view = convertView
                vh = view.tag as Holder
            }
            val item = items[pos]

            vh.text.text = item.title

            return view
        }

        override fun getItem(pos: Int) = items[pos]

        override fun getItemId(pos: Int) = items[pos].id.toLong()

        override fun getCount() = items.size

        private class Holder(row: View?) {
            var text: MyTextView = row?.findViewById(R.id.text) as MyTextView
        }
    }

    data class SpinnerDataModel(
            val title: String,
            var id: Int
    )


}