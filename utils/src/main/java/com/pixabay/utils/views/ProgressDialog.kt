package com.pixabay.utils.views

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.pixabay.utils.R

class ProgressDialog {
    lateinit var mDialog: Dialog

    companion object {
        private var instance: ProgressDialog? = null
        fun get(): ProgressDialog {
            return (instance
                    ?: ProgressDialog().also {
                        instance = it
                    })
        }
    }

    fun show(context: Context?, cancelable: Boolean = true) {
        if (context == null) return
        mDialog = Dialog(context)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.window?.setBackgroundDrawableResource(android.R.color.transparent);
        mDialog.setContentView(R.layout.loading)
        mDialog.setCanceledOnTouchOutside(cancelable)
        mDialog.show()
    }

    fun hide() {
        if (::mDialog.isInitialized) {
            mDialog.dismiss()
        }
    }

}